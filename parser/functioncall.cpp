#include "functioncall.h"
#include "identifier.h"
#include "number.h"
#include "expression.h"
#include "stringliteral.h"
#include "parserconfig.h"
#include "boosthelper.h"
#include "astadapted.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;	
	
	struct sized_expression_class;
	struct return_argument_class;
	struct load_argument_class;
	struct argument_class;
	
	using sized_expression_type = x3::rule<sized_expression_class, ast::constant>;
	using return_argument_type = x3::rule<return_argument_class, ast::return_argument>;
	using load_argument_type = x3::rule<load_argument_class, ast::load_argument>;
	using argument_type = x3::rule<argument_class, ast::argument>;
	
	sized_expression_type const sized_expression = "sized_expression";
	return_argument_type const return_argument = "return_argument";
	load_argument_type const load_argument = "load_argument";
	argument_type const argument = "argument";
	return_statement_type const return_statement = "return_statement";
	function_call_type const function_call = "function_call";
	
	auto const sized_expression_def = 
			(x3::lit("QWORD") >> x3::lit('(') >> qword_parser() >> x3::lit(')'))
		|	(x3::lit("DWORD") >> x3::lit('(') >> dword_parser() >> x3::lit(')'))
		|	(x3::lit("WORD") >> x3::lit('(') >> word_parser() >> x3::lit(')'))
		|	(x3::lit("BYTE") >> x3::lit('(') >> byte_parser() >> x3::lit(')'))
		;
		
	auto const return_argument_def = x3::lit('&') > x3::string("return");
		
    auto const load_argument_def = 
			x3::lit('[') >> expression_parser() >> x3::lit(']');
        ;
		
	auto const argument_def =
			load_argument
		|	return_argument
		|	string_literal_parser()
		|	sized_expression
		|	expression_parser()
		;
		
	auto const return_statement_def =
			x3::lit("return")
		>>  argument
		>> 	x3::lit(';')
		;
		
	auto const function_call_def = 
			(namespaced_identifier_parser() | (x3::attr(std::string("*global")) >> identifier_parser()))
		>> 	x3::lit('(') 
		>> 	-(argument % x3::lit(','))
		>> 	x3::lit(')') 
		>> 	x3::lit(';')
		;
	
	SPIRIT_DEFINE_AUTO(
		  function_call
		, return_statement
		, sized_expression
		, return_argument
		, load_argument
		, argument
	);
	
	function_call_type const& function_call_parser(void)
	{
		return function_call;
	}
	
	return_statement_type const& return_statement_parser(void)
	{
		return return_statement;
	}
	
	BOOST_SPIRIT_INSTANTIATE(function_call_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(return_statement_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool
