#include "variable.h"
#include "expression.h"
#include "identifier.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct variable_expression_class;
	using variable_expression_type = x3::rule<variable_expression_class, boost::optional<ast::expression>>;
	
	variable_expression_type const variable_expression = "variable_expression";	
	variable_type const variable = "variable";
	
namespace
{	
	auto set_namespace_none = [&](auto& ctx){ x3::_val(ctx) = boost::none; };
	auto set_namespace = [&](auto& ctx){ x3::_val(ctx) = x3::_attr(ctx); };
}

	auto const variable_expression_def = ((x3::lit('=') > expression_parser())[set_namespace] | x3::eps[set_namespace_none]);

	auto const variable_def = 
	x3::lit("variable") 
	>> identifier_parser() 
	>> variable_expression
	>> x3::lit(';')
	;
	
	SPIRIT_DEFINE_AUTO(variable, variable_expression);	
	
	// parser exposure
	variable_type const& variable_parser(void) { return variable; }
	
	BOOST_SPIRIT_INSTANTIATE(variable_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool