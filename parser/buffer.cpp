#include "buffer.h"
#include "number.h"
#include "expression.h"
#include "identifier.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct no_data_buffer_class;
	struct size_data_buffer_class;
	struct data_buffer_class;
	
	using no_data_buffer_type = x3::rule<no_data_buffer_class, ast::expression>;
	using size_data_buffer_type = x3::rule<size_data_buffer_class, ast::buffer_data>;
	using data_buffer_type = x3::rule<data_buffer_class, std::vector<ast::u8>>;
	
	buffer_type const buffer = "buffer";
	no_data_buffer_type const no_data_buffer = "no_data_buffer";
	size_data_buffer_type const size_data_buffer = "size_data_buffer";
	data_buffer_type const data_buffer = "data_buffer";
	
	auto const no_data_buffer_def = 
		x3::lit('[') >> expression_parser() >> x3::lit(']') 
		>> x3::lit(';')
		;
		
	auto const size_data_buffer_def = 
		x3::lit('[') >> expression_parser() >> x3::lit(']') 
		>> x3::lit('=') 
		>> x3::lit('{') >> (byte_parser() % x3::lit(',')) >> x3::lit('}') 
		>> x3::lit(';')
		;
	
	auto const data_buffer_def = 
		x3::lit('[') >> x3::lit(']') 
		>> x3::lit('=') 
		>> x3::lit('{') >> (byte_parser() % x3::lit(',')) >> x3::lit('}') 
		>> x3::lit(';')
		;
	
	auto const buffer_def = 
	x3::lit("buffer") 
	>> identifier_parser() 
	>> (
	  no_data_buffer 
	| size_data_buffer 
	| data_buffer
	)
	;
	
	SPIRIT_DEFINE_AUTO(
		  buffer
		, no_data_buffer
		, size_data_buffer
		, data_buffer
		);	
	
	// parser exposure
	buffer_type const& buffer_parser(void) { return buffer; }
	
	BOOST_SPIRIT_INSTANTIATE(buffer_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool