#include "expression.h"
#include "identifier.h"
#include "constant.h"
#include "parserconfig.h"
#include "boosthelper.h"
#include "astadapted.h"

namespace roptool { namespace parser
{
	struct unary_operators_ : x3::symbols<ast::unary_operator>
	{
		unary_operators_()
		{
			add
				("+", ast::unary_operator::positive)
				("-", ast::unary_operator::negative)
				("~", ast::unary_operator::ones_complement)
			;
		};
	} unary_operators;
	
	struct additive_operators_ : x3::symbols<ast::expression_operator>
	{
		additive_operators_()
		{
			add
				("+", ast::expression_operator::addition)
				("-", ast::expression_operator::subtraction)
			;
		};
		
	} additive_operators;
	
	struct multiplicative_operators_ : x3::symbols<ast::expression_operator>
	{
		multiplicative_operators_()
		{
			add
				("*", ast::expression_operator::multiplication)
				("/", ast::expression_operator::division)
			;
		};
	} multiplicative_operators;	
	
	struct additive_expr_class;
	struct multiplicative_expr_class;
	struct unary_expr_class;
	struct primary_expr_class;
	
	using additive_expr_type = x3::rule<additive_expr_class, ast::expression>;
	using multiplicative_expr_type = x3::rule<multiplicative_expr_class, ast::expression>;
	using unary_expr_type = x3::rule<unary_expr_class, ast::operand>;
	using primary_expr_type = x3::rule<primary_expr_class, ast::operand>;
	
	additive_expr_type const additive_expr = "additive_expr";
	multiplicative_expr_type const multiplicative_expr = "multiplicative_expr";
	unary_expr_type const unary_expr = "unary_expr";
	primary_expr_type const primary_expr = "primary_expr";
	expression_type const expression = "expression";
	
	auto const additive_expr_def = multiplicative_expr 
		>> *(additive_operators > multiplicative_expr)
		;
		
	auto const multiplicative_expr_def = unary_expr 
		>> *(multiplicative_operators > unary_expr) 
		;
		
    auto const unary_expr_def =
            primary_expr
        |   (unary_operators > primary_expr)
        ;	
		
	std::string global_namespace = "*global";
	
	auto const primary_expr_def =
			constant_parser()
		|	(namespaced_identifier_parser() | (x3::attr(global_namespace) >> identifier_parser()))
		|	(x3::lit('(') > expression > x3::lit(')'))
		;
		
	auto const expression_def = additive_expr;
	
	SPIRIT_DEFINE_AUTO(
		  expression
		, additive_expr
		, multiplicative_expr
		, unary_expr
		, primary_expr
	);
	
	expression_type const& expression_parser(void)
	{
		return expression;
	}
	
	BOOST_SPIRIT_INSTANTIATE(expression_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool
