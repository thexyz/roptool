#include "datasection.h"
#include "identifier.h"
#include "symbol.h"
#include "functiondef.h"
#include "buffer.h"
#include "variable.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct data_namespace_class;
	using data_namespace_type = x3::rule<data_namespace_class, boost::optional<ast::identifier>>;
	
	data_namespace_type const data_namespace = "data_namespace";
	data_section_type const data_section = "data_section";
	
namespace
{
	auto set_namespace_none = [&](auto& ctx){ x3::_val(ctx) = boost::none; };
	auto set_namespace = [&](auto& ctx){ x3::_val(ctx) = x3::_attr(ctx); };
}
	
	auto const data_namespace_def = ((x3::lit(':') > identifier_parser())[set_namespace] | x3::eps[set_namespace_none]);
	
	auto const data_section_def = 
	x3::lit("data")
	> data_namespace
	>> x3::lit('{')
	>> *(symbol_parser() | functiondef_parser() | buffer_parser() | variable_parser())
	>> x3::lit('}')
	;
	
	SPIRIT_DEFINE_AUTO(data_section, data_namespace);	
	
	// parser exposure
	data_section_type const& data_section_parser(void) { return data_section; }
	
	BOOST_SPIRIT_INSTANTIATE(data_section_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool