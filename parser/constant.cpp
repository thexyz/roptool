#include "constant.h"
#include "number.h"
#include "parserconfig.h"
#include "boosthelper.h"

#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser 
{
	namespace x3 = boost::spirit::x3;
	
	auto fail_action = [](auto bit)
	{
		return [bit](auto& ctx)
		{
			if (architecture_abi_len() != bit)
				x3::_pass(ctx) = false;	
		};
	};
	
	constant_type const constant = "constant";
	
	auto constant_def = constant_type{} %=
		( qword_parser()[fail_action(bit_length::_64)] 
		| dword_parser()[fail_action(bit_length::_32)]
		| word_parser()[fail_action(bit_length::_16)]
		| byte_parser()[fail_action(bit_length::_8)]
		);

	SPIRIT_DEFINE_AUTO(constant);
	
	constant_type const& constant_parser(void)
	{
		return constant;
	}
	
	BOOST_SPIRIT_INSTANTIATE(constant_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool
