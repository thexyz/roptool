#include "identifier.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	identifier_type const identifier = "identifier";
	namespaced_identifier_type const namespaced_identifier = "namespaced_identifier";
	
	auto const identifier_def = x3::raw[x3::lexeme[(x3::alpha | '_') >> *(x3::alnum | '_')]];
	auto const namespaced_identifier_def = x3::lexeme[identifier >> x3::lit("::") > identifier];
	
	SPIRIT_DEFINE_AUTO(identifier, namespaced_identifier);	
	
	// parser exposure
	identifier_type const& identifier_parser(void) { return identifier; }
	namespaced_identifier_type const& namespaced_identifier_parser(void) { return namespaced_identifier; }
	
	BOOST_SPIRIT_INSTANTIATE(identifier_type, iterator_type, context_type);
	BOOST_SPIRIT_INSTANTIATE(namespaced_identifier_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool