#ifndef BOOSTHELPER_H
#define BOOSTHELPER_H

#if (_MSC_VER == 1900)
// BUG in decltype(auto) means we have to work around BOOST_SPIRIT_DEFINE
#include <boost/spirit/home/x3/support/unused.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/variadic/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/cat.hpp>

#define SPIRIT_DEFINE_WITH_TYPE_AND_INIT(rule_name, rule_type, init_func)         \
  template <typename Iterator, typename Context, typename Attribute>              \
  bool parse_rule(rule_type, Iterator& first, Iterator const& last,               \
                         Context const& context, Attribute& attr)                 \
    {                                                                             \
      using boost::spirit::x3::unused;                                            \
      static auto const def_ = (rule_name = init_func());                         \
      return def_.parse(first, last, context, unused, attr);                      \
    }                                                                             \
  /***/
  
#define SPIRIT_DEFINE_AUTO_(r, data, rule_name) \
	SPIRIT_DEFINE_WITH_TYPE_AND_INIT(rule_name, BOOST_PP_CAT(rule_name, _type), [&]() { return BOOST_PP_CAT(rule_name, _def); } )	\
    /***/
	
#define SPIRIT_DEFINE_AUTO(...) BOOST_PP_SEQ_FOR_EACH(                         \
    SPIRIT_DEFINE_AUTO_, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))             \
    /***/
#else
#define SPIRIT_DEFINE_AUTO BOOST_SPIRIT_DEFINE
#endif

#endif // BOOSTHELPER_H
