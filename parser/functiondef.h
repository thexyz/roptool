#ifndef FUNCTIONDEF_H
#define FUNCTIONDEF_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct functiondef_class;
	
	using functiondef_type = x3::rule<functiondef_class, ast::functiondef_decl>;

	BOOST_SPIRIT_DECLARE(functiondef_type);

	functiondef_type const& functiondef_parser(void);
} // namespace parser
} // namespace roptool

#endif // FUNCTIONDEF_H
