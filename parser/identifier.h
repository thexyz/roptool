#ifndef IDENTIFIER_H
#define IDENTIFIER_H

#include "ast.h"
#include "astadapted.h"
#include "boosthelper.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct identifier_class;
	struct namespaced_identifier_class;
	
	using identifier_type = x3::rule<identifier_class, ast::identifier>;
	using namespaced_identifier_type = x3::rule<namespaced_identifier_class, ast::namespace_identifier>;

	BOOST_SPIRIT_DECLARE(identifier_type);
	BOOST_SPIRIT_DECLARE(namespaced_identifier_type);

	identifier_type const& identifier_parser(void);
	namespaced_identifier_type const& namespaced_identifier_parser(void);
} // namespace parser
} // namespace roptool

#endif // IDENTIFIER_H
