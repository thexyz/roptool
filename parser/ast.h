#ifndef AST_H
#define AST_H

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/ast/variant.hpp>
#include <boost/optional/optional_io.hpp>
#include <boost/serialization/strong_typedef.hpp>

#include <list>
#include <vector>
#include <cstdint>

namespace roptool { namespace ast
{
    namespace x3 = boost::spirit::x3;
	
	struct unary_operand;
	struct expression;
	struct operand;
	struct constant;
	struct argument;
	
	using u64 = uint64_t;
	using u32 = uint32_t;
	using u16 = uint16_t;
	using u8 = uint8_t;
	
	using identifier = std::string;
	using string_literal = std::string;
	using sized_expression = constant;
	using return_statement = argument;

	struct return_argument
	{
		std::string name;
	};

	struct namespace_identifier
	{
		identifier namespace_;
		identifier name;
	};
	
    struct constant :
        x3::variant<
            u64
		  , u32
		  , u16
		  , u8
        >
    {
        using base_type::base_type;
        using base_type::operator=;
    };
	
	struct operand :
		x3::variant<
			  constant
			, namespace_identifier
			, x3::forward_ast<unary_operand>
			, x3::forward_ast<expression>
			>
	{
        using base_type::base_type;
        using base_type::operator=;
	};
	
	enum class unary_operator
	{
		positive,
		negative,
		ones_complement
	};
	
	struct unary_operand
	{
		unary_operator operator_;
		operand operand_;
	};
	
	enum class expression_operator
	{
		addition,
		subtraction,
		multiplication,
		division
	};
	
	struct operation
    {
        expression_operator operator_;
        operand operand_;
    };
	
	struct expression
	{
        operand first;
        std::list<operation> rest;
	};
	
	struct load_argument
	{
		expression expr;
	};
	
    struct argument :
        x3::variant<
            load_argument
		  , return_argument
		  , string_literal
		  , sized_expression
		  , expression
        >
    {
        using base_type::base_type;
        using base_type::operator=;
    };
	
	struct function_call
	{
		namespace_identifier name;
		std::list<argument> arguments;
	};
	
	struct buffer_data
	{
		expression size;
		std::vector<u8> data;
	};
	
	struct buffer_data_type :
		x3::variant<
			  expression
			, std::vector<u8>
			, buffer_data
		>
	{
		using base_type::base_type;
		using base_type::operator=;
	};
	
	struct buffer_decl
	{
		identifier name;
		buffer_data_type data;
	};
	
	struct functiondef_decl
	{
		identifier name;
		expression value;
	};
	
	struct symbol_data :
		x3::variant<
			  expression
			, string_literal
		>
	{
		using base_type::base_type;
		using base_type::operator=;
	};
	
	struct symbol_decl
	{
		identifier name;
		symbol_data value;
	};
		
	struct variable_decl
	{
		identifier name;
		boost::optional<expression> value;
	};
	
	struct data_section_entry : 
		x3::variant<
			  symbol_decl
			, functiondef_decl
			, buffer_decl
			, variable_decl
		>
	{
		using base_type::base_type;
		using base_type::operator=;
	};
	
	struct data_section
	{
		boost::optional<identifier> namespace_;
		std::list<data_section_entry> entries;
	};
	
	struct statement :
		x3::variant<
			  function_call
			, return_statement
	//		, if_statement
	//		, while_loop
	//		, loop
		>
	{
		using base_type::base_type;
		using base_type::operator=;
	};
	
	struct code_parameter :
		x3::variant<
			  expression
			, identifier
		>
	{
		using base_type::base_type;
		using base_type::operator=;
	};

	struct code_section
	{
		std::string identifier;
		std::list<code_parameter> parameters;
		std::list<statement> statements;
	};
	
	struct ropscript_entry :
		x3::variant<
			  data_section
			, code_section
		>
	{
		using base_type::base_type;
		using base_type::operator=;
	};
	
	using ropscript = std::list<ropscript_entry>;
	
	/*
	struct nil {};
    struct signed_;
    struct expression;
    struct function_call;

	struct primary_expression :
        x3::variant<
            identifier
		  , constant
		  , string_literal
          , x3::forward_ast<expression>
        >
	{
		
	};

    struct operand :
        x3::variant<
          , u64
		  , u32
		  , u16
		  , u8
		  , std::string
		  , return_token
		  , data_reference
		  , sized_token
          , x3::forward_ast<signed_>
          , x3::forward_ast<expression>
        >
    {
        using base_type::base_type;
        using base_type::operator=;
    };
	
    struct signed_
    {
        char sign;
        operand operand_;
    };

    struct operation
    {
        char operator_;
        operand operand_;
    };
	
	struct expression
	{
        operand first;
        std::list<operation> rest;
	};

	struct qword_expression
    {
		expression expr;
    };

	struct dword_expression
    {
		expression expr;
    };
	
	struct word_expression
    {
		expression expr;
    };
	
	struct byte_expression
    {
		expression expr;
    };
	
	struct sized_expression :
        x3::variant<
          , qword_expression
		  , dword_expression
		  , word_expression
		  , byte_expression
        >
    {
        using base_type::base_type;
        using base_type::operator=;
    };
	
    struct argument :
        x3::variant<
            load_argument
		  , return_argument
		  , string_literal
		  , sized_expression
		  , expression
        >
    {
        using base_type::base_type;
        using base_type::operator=;
    };

	primary_expression = identifier | number | ('(' > expression > ')');
	load_argument = expression;
	argument = load_argument | return_argument | string_literal | expression;
	argument_list = argument % x3::lit(',');
	
	struct variable_decl
	{
		std::string identifier;
		expression value;
	};
	
	struct buffer_decl
	{
		std::string identifier;
		expression size;
	};
	
	struct function_decl
	{
		std::string identifier;
		expression value;
	};
	
	struct symbol_decl
	{
		std::string identifier;
		expression value;
	};
	
	struct data_section : 
		x3::variant<
			  symbol_decl
			, function_decl
			, buffer_decl
			, variable_decl
		>
	{
		using base_type::base_type;
		using base_type::operator=;
	};
	
	struct function_call
	{
		std::string name;
		std::list<parameter> parameters;
	};
	
	struct if_statement
	{
		expression expression;
		std::list<statement> statements;
	};
	
	struct statement :
		x3::variant<
			  function_call
//			, if_statement
//			, while_loop
//			, loop
		>
	{
		using base_type::base_type;
		using base_type::operator=;
	};
	
	struct code_section
	{
		std::string identifier;
		std::list<parameter> parameters;
		std::list<statement> statements;
	};
	
	struct script
	{
		data_section data;
		std::list<code_section> code_sections;
	};
	
	struct parameter : x3::variant<
		  nil
		, u64
		, u32
		, u16
		, u8
		,
	{
		
	};
	
	struct qword_parameter : x3::variant<
		  u64
		, std::string
		, 
    // AST1_VISIT_BEGIN
    struct nil {};
    struct signed_;
    struct expression;
    struct function_call;

    struct operand :
        x3::variant<
            nil
          , double
          , x3::forward_ast<signed_>
          , x3::forward_ast<expression>
          , x3::forward_ast<function_call>
        >
    {
        using base_type::base_type;
        using base_type::operator=;
    };
    // AST1_VISIT_END

    // AST2_VISIT_BEGIN
    struct signed_
    {
        char sign;
        operand operand_;
    };

    struct operation : x3::position_tagged
    {
        char operator_;
        operand operand_;
    };

    struct expression : x3::position_tagged
    {
        operand first;
        std::list<operation> rest;
    };
*/
} // namespace ast
} // namespace roptool

#endif // AST_H
