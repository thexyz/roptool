#ifndef CODESECTION_H
#define CODESECTION_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct code_section_class;
	using code_section_type = x3::rule<code_section_class, ast::code_section>;

	BOOST_SPIRIT_DECLARE(code_section_type);

	code_section_type const& code_section_parser(void);
} // namespace parser
} // namespace roptool

#endif // CODESECTION_H
