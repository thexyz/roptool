#ifndef PARSERCONFIG_H
#define PARSERCONFIG_H

#include "skipper.h"
#include "error_handler.hpp"

#include <boost/spirit/include/support_multi_pass.hpp>
#include <boost/spirit/include/classic_position_iterator.hpp>

namespace roptool { namespace parser 
{
	namespace x3 = boost::spirit::x3;
	
	using base_iterator_type = std::istreambuf_iterator<char>;
	using forward_iterator_type = boost::spirit::multi_pass<base_iterator_type>;
	using pos_iterator_type = boost::spirit::classic::position_iterator2<forward_iterator_type>;
	using iterator_type = pos_iterator_type;
	using phrase_context_type = x3::phrase_parse_context<skipper_type>::type;
	using error_handler_type = error_handler<iterator_type>;
	using context_type = x3::with_context<
		  error_handler_tag
		, std::reference_wrapper<error_handler_type> const
		, phrase_context_type>::type;
		
	enum bit_length
	{
		_8 = 8,
		_16 = 16,
		_32 = 32,
		_64 = 64
	};
	
	void set_architecture_abi_len(bit_length length);
	bit_length architecture_abi_len(void);
} // namespace parser
} // namespace roptool

#endif // PARSERCONFIG_H
