#include "stringliteral.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	namespace 
	{	
		x3::uint_parser<ast::u8, 16, 1, 2> const p_hex8;
		x3::uint_parser<ast::u8, 8> const p_oct8;
	}
	
	struct escape_sequences_ : x3::symbols<char>
	{
		escape_sequences_()
		{
			add
				("a", '\a')
				("b", '\b')
				("f", '\f')
				("n", '\n')
				("r", '\r')
				("t", '\t')
				("v", '\v')
				("\\", '\\')
				("\'", '\'')
				("\"", '\"')
				("\?", '\?')
			;
		};
		
	} escape_sequences;
	
	struct escape_sequence_class;
	struct octal_escape_sequence_class;
	struct hexadecimal_escape_sequence_class;
	
	using escape_sequence_type = x3::rule<escape_sequence_class, char>;
	using octal_escape_sequence_type = x3::rule<octal_escape_sequence_class, char>;
	using hexadecimal_escape_sequence_type = x3::rule<hexadecimal_escape_sequence_class, char>;
	
	string_literal_type const string_literal = "string_literal";
	escape_sequence_type const escape_sequence = "escape_sequence";
	octal_escape_sequence_type const octal_escape_sequence = "octal_escape_sequence";
	hexadecimal_escape_sequence_type const hexadecimal_escape_sequence = "hexadecimal_escape_sequence";
	
	auto const string_literal_def = x3::lexeme[x3::lit('"') >> *(escape_sequence | (x3::char_ - (x3::lit('\\') | x3::lit('"')))) >> x3::lit('"')];
	auto const octal_escape_sequence_def = x3::lit('x') >> p_hex8;
	auto const hexadecimal_escape_sequence_def = p_oct8;
	
	auto const escape_sequence_def = 
		x3::lit('\\') 
		>> (escape_sequences 
		| 	octal_escape_sequence
		|	hexadecimal_escape_sequence );
	
	SPIRIT_DEFINE_AUTO(
		  string_literal
		, escape_sequence
		, octal_escape_sequence
		, hexadecimal_escape_sequence
		);
	
	string_literal_type const& string_literal_parser(void) { return string_literal; }
	BOOST_SPIRIT_INSTANTIATE(string_literal_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool
