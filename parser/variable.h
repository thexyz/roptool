#ifndef VARIABLE_H
#define VARIABLE_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct variable_class;
	using variable_type = x3::rule<variable_class, ast::variable_decl>;

	BOOST_SPIRIT_DECLARE(variable_type);

	variable_type const& variable_parser(void);
} // namespace parser
} // namespace roptool

#endif // VARIABLE_H
