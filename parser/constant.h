#ifndef CONSTANT_H
#define CONSTANT_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser 
{
	namespace x3 = boost::spirit::x3;
	
	struct constant_class;
	using constant_type = x3::rule<constant_class, roptool::ast::constant>;
	
	BOOST_SPIRIT_DECLARE(constant_type);
	
	constant_type const& constant_parser(void);
} // namespace parser
} // namespace roptool

#endif // CONSTANT_H
