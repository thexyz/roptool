#ifndef FUNCTIONCALL_H
#define FUNCTIONCALL_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct function_call_class;
	struct return_statement_class;
	
	using function_call_type = x3::rule<function_call_class, ast::function_call>;
	using return_statement_type = x3::rule<return_statement_class, ast::return_statement>;
	
	BOOST_SPIRIT_DECLARE(function_call_type);
	BOOST_SPIRIT_DECLARE(return_statement_type);
	
	function_call_type const& function_call_parser(void);
	return_statement_type const& return_statement_parser(void);
} // namespace parser
} // namespace roptool

#endif // FUNCTIONCALL_H
