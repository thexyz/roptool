#include "functiondef.h"
#include "expression.h"
#include "identifier.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	functiondef_type const functiondef = "functiondef";
	
	auto const functiondef_def = 
	x3::lit("function") 
	>> identifier_parser() 
	>> x3::lit('=')
	>> expression_parser()
	>> x3::lit(';')
	;
	
	SPIRIT_DEFINE_AUTO(functiondef);	
	
	// parser exposure
	functiondef_type const& functiondef_parser(void) { return functiondef; }
	
	BOOST_SPIRIT_INSTANTIATE(functiondef_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool