#include "ropscript.h"
#include "datasection.h"
#include "codesection.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;

	ropscript_type const ropscript = "ropscript";
	
	auto const ropscript_def = *(data_section_parser() | code_section_parser());
	
	SPIRIT_DEFINE_AUTO(ropscript);	
	
	// parser exposure
	ropscript_type const& ropscript_parser(void) { return ropscript; }
	
	BOOST_SPIRIT_INSTANTIATE(ropscript_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool