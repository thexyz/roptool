#ifndef SYMBOL_H
#define SYMBOL_H

#include "ast.h"
#include <boost/spirit/home/x3.hpp>

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	struct symbol_class;
	struct namespaced_identifier_class;
	
	using symbol_type = x3::rule<symbol_class, ast::symbol_decl>;

	BOOST_SPIRIT_DECLARE(symbol_type);

	symbol_type const& symbol_parser(void);
} // namespace parser
} // namespace roptool

#endif // SYMBOL_H
