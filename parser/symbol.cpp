#include "symbol.h"
#include "stringliteral.h"
#include "expression.h"
#include "identifier.h"
#include "astadapted.h"
#include "parserconfig.h"
#include "boosthelper.h"

namespace roptool { namespace parser
{
	namespace x3 = boost::spirit::x3;
	
	symbol_type const symbol = "symbol";
	
	auto const symbol_def = 
	x3::lit("symbol") 
	>> identifier_parser() 
	>> x3::lit('=')
	>> (expression_parser() | string_literal_parser())
	>> x3::lit(';')
	;
	
	SPIRIT_DEFINE_AUTO(symbol);	
	
	// parser exposure
	symbol_type const& symbol_parser(void) { return symbol; }
	
	BOOST_SPIRIT_INSTANTIATE(symbol_type, iterator_type, context_type);
} // namespace parser
} // namespace roptool