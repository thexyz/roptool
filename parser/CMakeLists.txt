###########################################################
##
## Project requires C++14
IF (CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    SET (CMAKE_CXX_FLAGS "-std=c++14 -fpermissive -static-libstdc++")
ELSEIF (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    SET (CMAKE_CXX_FLAGS "-std=c++14 -fpermissive")
ELSEIF (CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
	SET (CMAKE_CXX_FLAGS "/EHsc /W3 /D_CRT_SECURE_NO_WARNINGS /D_SCL_SECURE_NO_WARNINGS /wd4521")
ENDIF()

###########################################################

find_package(Boost COMPONENTS system filesystem unit_test_framework REQUIRED)

add_library(parser STATIC parserconfig.cpp expression.cpp constant.cpp number.cpp identifier.cpp symbol.cpp functiondef.cpp stringliteral.cpp buffer.cpp variable.cpp functioncall.cpp datasection.cpp codesection.cpp ropscript.cpp skipper.cpp)

