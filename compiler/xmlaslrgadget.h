#ifndef XMLASLRGADGET_H
#define XMLASLRGADGET_H

#include "XmlGadget.h"

template <typename T>
class XmlAslrGadget : public XmlGadget
{
public:
	XmlAslrGadget(T& table)
		: m_aslr_ref(nullptr)
	{
		visitor()->addHandler("gadget-aslr", [this, &table](const auto& aslr_base)
		{
			if (table.count(aslr_base))
			{
				m_aslr_base = table[aslr_base];
			}
			else
			{
				table[aslr_base] = m_aslr_base = std::make_shared<DataReference>();
			}
		}, true);
	}

	virtual ~XmlAslrGadget(void) = default;
	
	DataRefPtr address(void) const override
	{
		return m_aslr_ref;
	}

protected:
	void on_parse_complete(void) override
	{
		m_aslr_ref = std::make_shared<DataReference>();
        m_aslr_ref->set(m_aslr_base, XmlGadget::address(), DataReferenceOperation::ADDITION);
	}
	
private:
	DataRefPtr m_aslr_base;
	DataRefPtr m_aslr_ref;
};

#endif // XMLASLRGADGET_H
