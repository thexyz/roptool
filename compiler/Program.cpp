#include "Program.h"

#include <numeric>
#include <iostream>

namespace {
    void write_endian(std::vector<u8>& data, u64 word, int bits, bool little_endian)
    {
        // write endianness
        for (int k = 0; k < bits; k += 8)
        {
            // is little endian?
            if (little_endian)
            {
                // lsb first
                data.push_back((word >> k) & 0xFF);
            }
            else
            {
                // msb fist
                data.push_back((word >> (bits - k - 8)) & 0xFF);
            }
        }       
    }
}

std::ostream& operator<<(std::ostream& stream, const RopFileHeader& header)
{
    stream.write(reinterpret_cast<const char *>(&header), sizeof(RopFileHeader));
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const std::vector<u8>& data)
{
    std::ostream_iterator<u8> output_iterator(stream);
    std::copy(data.begin(), data.end(), output_iterator);
    return stream;
}

Program::Program(void)
    : m_sled_base(new DataReference)
{
    
}

void Program::setTarget(TargetPtr target)
{
    m_target = target;
	data().set_alignment(target->manifest()->arch_bitlen()/8);
}

DataSection& Program::data(void)
{
    return m_data_section;
}

RopSection& Program::code(void)
{
    return m_rop_section;
}

TargetPtr Program::target(void)
{
    return m_target;
}

void Program::addSled(u32 length)
{
    m_sled.clear();
    u64 value = 0;
    RelocationList relocs;
    if (m_target->manifest()->aslr())
    {
        auto aslr_table = m_target->aslr_table();
        relocs = create_relocation_for_aslr(aslr_table, m_target->getCallerGadget()->address());
        value = relocs[0].constant;
    }
    else
    {
        value = m_target->getCallerGadget()->address()->get();
    }
    
    const u8 *p = reinterpret_cast<const u8 *>(&value);
    
    // calculate the number of writes
    int writes = length;
    int arch_bytes = m_target->manifest()->arch_bitlen()/8;
    int align = m_target->manifest()->stack_alignment();
    while ((writes % arch_bytes) || (writes % align)) writes++;
    
    // add a sled of the specified length
    for (int i = 0; i < writes; i++)
    {
        m_sled.push_back(p[i % arch_bytes]);
        
        if (((i % 4) == 0) && m_target->manifest()->aslr())
        {
            for (auto& reloc : relocs)
            {
                reloc.offset = std::make_shared<DataReference>();
                reloc.offset->set(m_sled_base, i, DataReferenceOperation::ADDITION);
            }
            
            m_relocs.insert(m_relocs.end(), relocs.begin(), relocs.end());            
        }
    }
}

void Program::write(std::ostream& stream)
{
    RopFileHeader header;
    std::memset(reinterpret_cast<u8*>(&header), 0, sizeof(header));
    
    header.magic = ROPFILE_MAGIC;
    header.version = ROPFILE_VERSION(1, 1);
    
    // deal with rop section
    auto rop_binary = generate_rop_binary();
	
	std::cout << "sizes of sectoins: " << " data:" << data().data().size() << " - code:" << rop_binary.stack.size() << "\n";
    header.csize = m_sled.size() + rop_binary.stack.size();
    header.centry = m_target->getCallerGadget()->address()->get();
	
	// deal with data section
    header.dsize = data().size();
    header.daddr = data().base();
	
    rop_binary.base->set(sizeof(header) + data().data().size() + m_sled.size());
    
    std::cout << "got value for base: " << rop_binary.base->get() << "\n";
    std::vector<u8> relocs;

    // get the endianness 
    bool little_endian = true;
    
    auto data_relocs = data().relocations(m_target);
    data().data_section_base()->set(sizeof(header));
    
    m_relocs.insert(m_relocs.end(), data_relocs.begin(), data_relocs.end());
    m_relocs.insert(m_relocs.end(), rop_binary.relocs.begin(), rop_binary.relocs.end());
    
    m_sled_base->set(sizeof(header)+data().data().size());
    
    auto aslr_table = m_target->aslr_table();
    
    std::list<std::string> strtab;
    
    using InternalRelocationSymbolTable = struct
    {
        u32 id;
        DataRefPtr name;
    };
    
    std::map<std::string, InternalRelocationSymbolTable> symtab;
    auto strtab_base = std::make_shared<DataReference>();
    
    for (auto& reloc : m_relocs)
    {
        auto symbol_it = std::find_if(aslr_table.begin(), aslr_table.end(), [reloc](auto& entry)
        {
            return (entry.second.get() == reloc.aslr_ref);
        });
        
        if (symbol_it == aslr_table.end())
        {
            std::cout << "referenced symbol not found in ASLR table\n";
            BOOST_ASSERT(0);
        }
        
        if (!symtab.count(symbol_it->first))
        {
            std::cout << "found usage of symbol: " << symbol_it->first << "\n";
            InternalRelocationSymbolTable entry;
            
            entry.id = static_cast<u32>(symtab.size());
            entry.name = std::make_shared<DataReference>();
            entry.name->set(strtab_base, std::accumulate(strtab.begin(), strtab.end(), size_t{}, [](size_t& a, const std::string& b)
            {
                return a + b.length()+1;
            }), DataReferenceOperation::ADDITION);
            
            symtab.insert(std::make_pair(symbol_it->first, entry));
            strtab.push_back(symbol_it->first);
        }
        
        auto& symtab_entry = symtab.at(symbol_it->first);
        
        u32 word = (symtab_entry.id << 16) | (u16)reloc.type;
        
        // write endianness
        for (int k = 0; k < m_target->manifest()->arch_bitlen(); k += 8)
        {
            // is little endian?
            if (little_endian)
            {
                // lsb first
                relocs.push_back(((u64)word >> k) & 0xFF);
            }
            else
            {
                // msb fist
                relocs.push_back(((u64)word >> (m_target->manifest()->arch_bitlen() - k - 8)) & 0xFF);
            }
        }
        
        // write endianness
        for (int k = 0; k < m_target->manifest()->arch_bitlen(); k += 8)
        {
            // is little endian?
            if (little_endian)
            {
                // lsb first
                relocs.push_back((reloc.offset->get() >> k) & 0xFF);
            }
            else
            {
                // msb fist
                relocs.push_back((reloc.offset->get() >> (m_target->manifest()->arch_bitlen() - k - 8)) & 0xFF);
            }
        }  
    }
    
    std::vector<u8> symbol_table;
    
    strtab_base->set(sizeof(header) + data().size() + m_sled.size() + rop_binary.stack.size() + relocs.size() + symtab.size()*sizeof(RelocationSymbolTable));
    
    for (auto& entry : symtab)
    {
        RelocationSymbolTable table_entry;
        table_entry.id = entry.second.id;
        table_entry.strofs = entry.second.name->get();        
        
        write_endian(symbol_table, table_entry.id, 32, true);
        write_endian(symbol_table, table_entry.strofs, 32, true);
    }
    
    header.reloc_size = relocs.size();
    header.symtab_size = symbol_table.size();
    
    stream << header << data().data() << m_sled << rop_binary.stack << relocs << symbol_table;
    
    // write strtable
    for (auto& str : strtab)
    {
        stream << str << '\0';
    }
}

CompiledCode Program::generate_rop_binary(void)
{ 
    RopFunctionPtrList functions = code().functions();
    
    // find the entry function
    auto it = std::find_if(functions.begin(), functions.end(), [=](RopFunctionPtr func)
    {
        return (func->name() == "entry");
    });
    
    // if no entry then big problem
    if (it == functions.end())
    {
        throw std::runtime_error("No entry function defined");
    }
    
	std::list<std::pair<RopFunctionPtr, DataRefPtr>> m_used_rop_functions;
	
	// first we establish size of referenced values and create empty values
	for (auto& function : functions)
	{
		std::cout << "got function: " << function->name() << "(" << function->parameter_types() << ")" << " size:" << function->size(m_target) << "\n";
		if (function->data_reference_used())
		{
			auto code_ref = function->data_reference();
			auto size = function->size(m_target);
			auto ref = data().deferred_buffer(size);
			
			code_ref->set(ref);
			function->data_size_reference()->set(size);
			m_used_rop_functions.push_back(std::make_pair(function, ref));
		}
		else if (function->data_size_reference_used())
		{
			auto size = function->size(m_target);
			function->data_size_reference()->set(size);
		}
	}
	
	// second pass we write real binary data
	for (auto& function : m_used_rop_functions)
	{
        // TODO: relocs
		auto rop = function.first->binary(m_target);
		data().update(function.second, rop.stack.data(), rop.stack.size());
	}
	
    RopFunctionPtr func = *it;
    
    // we only support a single function currently
    return func->binary(m_target);
}
