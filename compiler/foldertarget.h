#ifndef FOLDERTARGET_H
#define FOLDERTARGET_H

// roptool
#include "Target.h"
#include "Gadget.h"
#include "GadgetMap.h"

// std
#include <string>
#include <vector>

class FolderTarget : public Target
{
public:
	FolderTarget(void);
	FolderTarget(const std::string& name);
	virtual ~FolderTarget(void) = default;
	
	// accessor/mutator
	const std::string& name(void) { return m_name; }
	void setName(const std::string& name);
	
	// 
	TargetManifestPtr manifest(void) { return m_manifest; }
	
	// get the best  gadget map
	GadgetMapPtr bestGadgetMap(const std::string& regex);
	
	// get the caller gadget
	GadgetPtr getCallerGadget(void);
	
	// check if target defines function
	bool isFunction(const std::string& function) const;
	
	const AslrTable& aslr_table(void) const override;
	AslrTable& aslr_table(void) override;
	
private:
	std::string m_name;
	std::string m_path;
	GadgetPtrList m_gadgets;
	GadgetMapPtrList m_gadgetmaps;
	TargetManifestPtr m_manifest;
	AslrTable m_aslr_table;
	
	void readManifest(void);
	void readGadgets(void);
	void readGadgetMaps(void);
};

#endif // FOLDERTARGET_H
