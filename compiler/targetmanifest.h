#ifndef TARGETMANIFEST_H
#define TARGETMANIFEST_H

// roptool
#include "Types.h"

// std
#include <string>
#include <memory>

class TargetManifest
{
public:
	virtual ~TargetManifest(void) = default;
	
	virtual bool parse(const std::string& target) = 0;
	
	virtual const std::string& version(void) const = 0;
	virtual WordLength arch_bitlen(void) const = 0;
	virtual int stack_alignment(void) const = 0;
	virtual const std::string& caller_gadget(void) const = 0;
	virtual bool aslr(void) const = 0;
};

using TargetManifestPtr = std::shared_ptr<TargetManifest>;

#endif // TARGETMANIFEST_H