#ifndef XMLTARGETMANIFEST_H
#define XMLTARGETMANIFEST_H

// roptool
#include "targetmanifest.h"
#include "XmlActionVisitor.h"

// std
#include <memory>

class XmlTargetManifest : public TargetManifest
{
public:
	XmlTargetManifest(void);
	
	// file setting
	bool parse(const std::string& file);
	
	const std::string& version(void) const { return m_version; }
	WordLength arch_bitlen(void) const { return m_bitlen; }

	int stack_alignment(void) const { return m_stack_alignment; }
	const std::string& caller_gadget(void) const { return m_caller_gadget; }
	
	bool aslr(void) const { return m_aslr; }
	
private:
	std::unique_ptr<XmlActionVisitor> m_visitor;
	
	std::string m_version;
	WordLength m_bitlen;
	int m_stack_alignment;
	std::string m_caller_gadget;
	bool m_aslr;
	
	void set_version(const std::string& version);
	void set_arch_bitlen(const std::string& bitlen);
	void set_stack_alignment(const std::string& stack_alignment);
	void set_caller_gadget(const std::string& caller_gadget);
};

using XmlTargetManifestPtr = std::shared_ptr<XmlTargetManifest>;

#endif // XMLTARGETMANIFEST_H