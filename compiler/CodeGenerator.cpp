#include "CodeGenerator.h"
#include "expressionreducer.h"

#include <iostream>
#include <algorithm>
#include <boost/range/adaptor/reversed.hpp>

#ifdef _MSC_VER
#include <intrin.h>
#endif // #ifdef _MSC_VER

namespace
{
	u64 bit_mask(int bits)
	{
		u64 mask = 0xFFFFFFFFFFFFFFFFUL;
		return mask >> (64 - bits); 
	}
	
	unsigned int ctz(u32 val)
	{
#ifdef _MSC_VER
    DWORD trailing_zero = 0;

    if (_BitScanForward( &trailing_zero, val ))
    {
        return trailing_zero;
    }
    else
    {
        // undefined
        return 32;
    }
#else
		return val ? __builtin_clz(val) : 32;
#endif // _MSC_VER
	}
}

struct BufferDataVisitor
{
	BufferDataVisitor(ExpressionReducer::IdentifierLookup lookup)
	{
		m_lookup = lookup;
	}

	void operator()(roptool::ast::expression const& ast)
	{
		// tie the value to an expression
		ExpressionReducer expression(m_lookup);

		auto rhs = expression(ast);
		
		if (!rhs->constant())
		{
			// TODO: this is a critical error! size must be constant!!
		}

		size = rhs->get();
	}

	void operator()(std::vector<u8> const& ast)
	{
		size = ast.size();
		data = ast;
	}

	void operator()(roptool::ast::buffer_data const& ast)
	{
		(*this)(ast.data);
		(*this)(ast.size);

		if (size < data.size())
		{
			// TODO: this is a critical error! size must be >= to the data
		}
	}

	u64 size{0};
	std::vector<u8> data;

private:
	ExpressionReducer::IdentifierLookup m_lookup;
};

struct SymbolDataVisitor
{
	SymbolDataVisitor(ProgramPtr program, ExpressionReducer::IdentifierLookup lookup)
	{
		m_gen_program = program;
		m_lookup = lookup;
	}

	DataRefPtr operator()(roptool::ast::expression const& ast)
	{
		// tie the value to an expression
		ExpressionReducer expression(m_lookup);
		return expression(ast);
	}

	DataRefPtr operator()(roptool::ast::string_literal const& ast)
	{
		return m_gen_program->data().add(ast);
	}

private:
	ProgramPtr m_gen_program;
	ExpressionReducer::IdentifierLookup m_lookup;
};

struct ArgumentVisitor
{
	ArgumentVisitor(std::vector<char>& param_type, DataRefPtrList& params, ProgramPtr program, TargetPtr target, DataRefPtr default_ref, ExpressionReducer::IdentifierLookup lookup)
		: m_param_type(param_type)
		, m_param(params)
		, m_gen_program(program)
		, m_target(target)
		, m_default_ref(default_ref)
		, m_lookup(lookup)
	{
	}

	void store_parameter_value(int param_bits, u64 value)
	{
		auto arch_bits = m_target->manifest()->arch_bitlen();
	    auto param_n = param_bits / arch_bits;
    
		if (param_n == 0)
			param_n = 1;

		// if there is more than one param we may need some dummy params
		if ((param_n > 1) && (m_param.size() % 2))
		{
			// align to register
			m_param_type.push_back('v');
			m_param.push_back(m_default_ref);
		}
		
		// create a list
		for (int i = 0; i < param_n; i++)
		{
			DataRefPtr ref = m_gen_program->data().add((value >> (i * arch_bits)) & bit_mask(arch_bits));
			
			// add reference to list
			m_param_type.push_back('v');
			m_param.push_back(ref);
		}	
	}

	void operator()(roptool::ast::u64 const& ast)
	{
		store_parameter_value(64, ast);
	}

	void operator()(roptool::ast::u32 const& ast)
	{
		store_parameter_value(32, ast);
	}

	void operator()(roptool::ast::u16 const& ast)
	{
		store_parameter_value(16, ast);
	}

	void operator()(roptool::ast::u8 const& ast)
	{
		store_parameter_value(8, ast);
	}

	void operator()(roptool::ast::load_argument const& ast)
	{
		// tie the value to an expression
		ExpressionReducer expression(m_lookup);

		auto rhs = expression(ast.expr);

		m_param_type.push_back('l');
		m_param.push_back(rhs);
	}

	void operator()(roptool::ast::return_argument const& ast)
	{
		std::cout << "got return type!\n";
		m_param_type.push_back('r');
		m_param.push_back(m_default_ref);
	}

	void operator()(roptool::ast::string_literal const& ast)
	{
		auto rhs = m_gen_program->data().add(ast);
		m_param_type.push_back('v');
		m_param.push_back(rhs);
	}

	void operator()(roptool::ast::sized_expression const& ast)
	{
		boost::apply_visitor(*this, ast);
	}

	void operator()(roptool::ast::expression const& ast)
	{
		// tie the value to an expression
		ExpressionReducer expression(m_lookup);

		auto rhs = expression(ast);

		m_param_type.push_back('v');
		m_param.push_back(rhs);
	}

private:
	std::vector<char>& m_param_type;
	DataRefPtrList& m_param;
	ProgramPtr m_gen_program;
	TargetPtr m_target;
	DataRefPtr m_default_ref;
	ExpressionReducer::IdentifierLookup m_lookup;
};

void CodeGenerator::operator()(roptool::ast::symbol_decl const& ast)
{
	SymbolDataVisitor sdv(m_gen_program, [this](auto& id)
	{
		return m_reference_table.at(std::make_pair(id.namespace_, id.name));
	});

	auto ref = boost::apply_visitor(sdv, ast.value);

	std::cout << "got symbol: " << ast.name << " value:" << ref->get() << " constant?: " << ref->constant() << "\n";
	m_reference_table[std::make_pair(m_namespace, ast.name)] = ref;
}

void CodeGenerator::operator()(roptool::ast::functiondef_decl const& ast)
{
	// tie the value to an expression
	ExpressionReducer expression([this](auto& id)
	{
		return m_reference_table.at(std::make_pair(id.namespace_, id.name));
	});

	auto rhs = expression(ast.value);

	m_reference_table[std::make_pair(m_namespace, ast.name)] = rhs;
	m_function_table[std::make_pair(m_namespace, ast.name)] = rhs;
}

void CodeGenerator::operator()(roptool::ast::buffer_decl const& ast)
{
	BufferDataVisitor bdv([this](auto& id)
	{
		return m_reference_table.at(std::make_pair(id.namespace_, id.name));
	});
	boost::apply_visitor(bdv, ast.data);

	auto& data = bdv.data;

	if (bdv.size > data.size())
	{
		// TODO: default value should be configurable
		data.insert(data.end(), bdv.size-data.size(), 0);
	}

	auto ref = m_gen_program->data().add(data.data(), data.size());
	m_reference_table[std::make_pair(m_namespace, ast.name)] = ref;
}

void CodeGenerator::operator()(roptool::ast::variable_decl const& ast)
{
	auto word_size = m_target->manifest()->arch_bitlen()/8;
	DataRefPtr ref;

	if (ast.value)
	{
		// tie the value to an expression
		ExpressionReducer expression([this](auto& id)
		{
			return m_reference_table.at(std::make_pair(id.namespace_, id.name));
		});

		auto rhs = expression(*ast.value);
		ref = m_gen_program->data().add(rhs, word_size);
	}
	else
	{
		// TODO: default should be configurable...
		u64 default_val = 0;
		ref = m_gen_program->data().add(&default_val, word_size);
	}

	// add to reference table
	m_reference_table[std::make_pair(m_namespace, ast.name)] = ref;
}

void CodeGenerator::operator()(roptool::ast::data_section const& ast)
{
	// TODO: move fixed string somewhere else?
	m_namespace = (ast.namespace_) ? *ast.namespace_ : "*global";

	for (auto const& oper : ast.entries)
		boost::apply_visitor(*this, oper);
}

void CodeGenerator::insert_call(const std::string& function, const std::string& types, const DataRefPtrList& list)
{
	std::string call_prototype = function + std::string("(") + types + ")";

	// lookup best map
	GadgetMapPtr map = m_target->bestGadgetMap(call_prototype);
	
	// check for null
	if (!map)
	{
		// no map found!
		throw std::runtime_error("Target has no support for call scheme: " + types);
	}

	RopFunctionCallPtr function_call(new RopFunctionCall);
	
	// set function call data
	function_call->setMap(map);
	function_call->setParameters(list);
	
	// if its not a function map, we need to give it a function
	if (!map->isFunction())
	{
		// TODO: fixup
		auto ref = m_function_table.at(std::make_pair("*global", function));
		function_call->setFunction(ref);
	}
	
	std::cout << "map size: " << map->size() << "\n";
	
	m_rop_func->add(function_call);	
}

DataRefPtr CodeGenerator::constant_ref(u64 constant)
{
	DataRefPtr ref;
	
	try
	{
		ref = m_constants.at(constant);
	}
	catch (const std::out_of_range& oor)
	{
		ref = std::make_shared<DataReference>(constant, true);
		m_constants.insert(std::make_pair(constant, ref));
	}
	
	return ref;
}

DataRefPtr CodeGenerator::additive_ref(DataRefPtr base, u64 constant)
{
	auto ref = std::make_shared<DataReference>();
    ref->set(base, constant, DataReferenceOperation::ADDITION);
	return ref;
}

#define DEFAULT_VALUE	(0x61616161)	

class CallerId
{
public:
	CallerId(RopFunctionPtr function, DataSection& data, u64 word_size)
		: m_function(function)
	{
		if (function->name() == "enfgdetry")
		{
			// root source does not have working stack buffers
			m_size = std::make_shared<DataReference>(0, true);
			m_base = std::make_shared<DataReference>(0, true);
			
			DataRefPtrList fake_buffers = { m_size, m_size };
			auto fake_working_stack = data.add(fake_buffers, word_size);
			DataRefPtrList caller_id = { m_base, m_size, fake_working_stack };

			// create a location to hold these values
			data.add(std::string("entry caller id"));
			m_reference = data.add(caller_id, word_size);
		}
		else
		{
			auto working_stack = function->working_stack();
			auto working_stack_index = function->working_stack_index();
			m_base = std::make_shared<DataReference>();
			m_size = std::make_shared<DataReference>();
			
			DataRefPtrList caller_id = { m_base, m_size, working_stack, working_stack_index };

			// create a location to hold these values
			data.add(std::string("caller id:")+function->name()+"("+function->parameter_types() +")");
			m_reference = data.add(caller_id, word_size);

			// TODO: replace this hack for getting a data reference used flags
			m_function->data_reference();
		}
	}
	
	DataRefPtr reference()
	{
		return m_reference;
	}
	
	void set_pivot(RopFunctionCallPtrList::iterator pivot)
	{
		m_pivot_point = pivot;
	}
	
	void resolve(TargetPtr target)
	{
		size_t size = 0, offset = 0;
		
		// loop through every function call
		auto it = std::next(m_pivot_point);
		
		std::for_each(it, m_function->calls().end(), [&size, target](const auto& call)
		{
			size += call->size(target);
			
			while (size % target->manifest()->stack_alignment())
			{
				size += target->manifest()->arch_bitlen()/8;
			}
		});
		
		// set the size
		m_size->set(size);
		
		it = std::next(m_pivot_point);
		
		// calculate size between pivot point and function start to get our offset
		std::for_each(m_function->calls().begin(), it, [&offset, target](const auto& call)
		{
			offset += call->size(target);
			
			while (offset % target->manifest()->stack_alignment())
			{
				offset += target->manifest()->arch_bitlen()/8;
			}
		});
		
		// set the copy base
        m_base->set(m_function->data_reference(), offset, DataReferenceOperation::ADDITION);
	}
	
private:
	DataRefPtr m_reference, m_base, m_size;
	RopFunctionCallPtrList::iterator m_pivot_point;
	RopFunctionPtr m_function;
};

void CodeGenerator::operator()(roptool::ast::function_call const& ast)
{
	auto namespace_ = m_namespace;
	
	ArgumentVisitor argument_visitor(m_param_type, m_param, m_gen_program, m_target, m_zero_ref, [namespace_, this](auto& id)
	{
		DataRefPtr ref;

		// if global, first check local scope
		if (id.namespace_ == "*global")
		{
			try
			{
				ref = m_reference_table.at(std::make_pair(namespace_, id.name));
				return ref;
			}
			catch (...)
			{
				// do nothing
			}
		}

		std::cout << "arg found:" << id.namespace_ << "::" << id.name << "\n";
		return m_reference_table.at(std::make_pair(id.namespace_, id.name));
	});

	std::cout << "func found:" << ast.name.namespace_ << "::" << ast.name.name << "\n";
	
	for (auto const& argument : ast.arguments)
		boost::apply_visitor(argument_visitor, argument);

	// add a null terminator and construct string
	m_param_type.push_back('\0');
	
	std::string param_type_list = std::string(m_param_type.data());
	std::string call_prototype = ast.name.name + std::string("(") + param_type_list + ")";
	std::cout << call_prototype << "\n";
	
	try
	{
		// lookup best map
		GadgetMapPtr map = m_target->bestGadgetMap(call_prototype);
		
		// check for null
		if (!map)
		{
			// throw out of bounds first, if found them runtime error
			m_function_table.at(std::make_pair(ast.name.namespace_, ast.name.name));
			
			// no map found!
			throw std::runtime_error("Target has no support for call scheme: " + param_type_list);
		}

		RopFunctionCallPtr function_call(new RopFunctionCall);
		
		// set function call data
		function_call->setMap(map);
		function_call->setParameters(m_param);
		
		// if its not a function map, we need to give it a function
		if (!map->isFunction())
		{
			// TODO: fixup
			auto ref = m_function_table.at(std::make_pair(ast.name.namespace_, ast.name.name));
			function_call->setFunction(ref);
		}
		
		m_rop_func->add(function_call);	
	}
	catch (const std::out_of_range& oor)
	{
		// check if function is ROP function table
		if (!m_rop_function_table.count(ast.name.name))
		{
			throw;
		}
		
		auto call_param = m_param;
		auto call_param_type = m_param_type;
		auto function_param_count = m_variable_param_count;
		auto insertion_pos = m_rop_func->calls().size(); 
		auto rop_function = m_rop_func;
		
		// if we are calling a function that has not yet been defined yet we need to defer this insertions
		// there is no concept of forward declaration
		auto insert_rop_call = [call_prototype, call_param, call_param_type, function_param_count, insertion_pos, rop_function, &ast, this](auto insert_call)
		{
			// we need to inject instructions
			auto word_size = m_target->manifest()->arch_bitlen()/8;

			unsigned int work_counter = 0;
			auto default_value = this->constant_ref(DEFAULT_VALUE);
			auto found_uses = false;
			
			m_insertion_pos = insertion_pos;
			
			// store current return value
			// TODO: optimise this out if not needed
			auto return_storage = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			auto tmp = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			insert_call("store", "rv", { default_value, return_storage });
	
			auto function_base = work_counter;
			auto functions = m_rop_function_table.equal_range(ast.name.name);
			std::list<RopFunctionPtr> applicable_functions;

			for (auto it = functions.first; it != functions.second; ++it)
			{
				// got function:
				if ((*it).second->parameters().size() == call_param.size())
				{
					std::cout << "got functionsss: " << (*it).second->name() << "(" << (*it).second->parameter_types() << ")" << "\n";
					// store the address and size of our rop function plus info on working stack
					insert_call("store", "vv", { (*it).second->data_reference(), this->additive_ref(m_local_work_ref, (work_counter++)*word_size) });
					insert_call("store", "vv", { (*it).second->data_size_reference(), this->additive_ref(m_local_work_ref, (work_counter++)*word_size) });
					insert_call("store", "vv", { (*it).second->working_stack_index(), this->additive_ref(m_local_work_ref, (work_counter++)*word_size) });
					insert_call("store", "vv", { (*it).second->working_stack(), this->additive_ref(m_local_work_ref, (work_counter++)*word_size) });
					applicable_functions.push_back((*it).second);
				}
			}
			
			if (applicable_functions.empty())
			{
				throw std::runtime_error("no applicable function for ROP call: \"" + call_prototype + "\" found." );
			}
			
			if (applicable_functions.size() > 1)
			{
				// not yet
				if (applicable_functions.size() > 2)
				{
					throw std::runtime_error("more than 2 overloaded functions are not supported.");
				}
				
				auto ctr = 0;
				for (auto& function : applicable_functions)
				{
					auto param_n = function->parameter_types().find('k');
					
					if (param_n != std::string::npos)
					{
						// constant found
						if (std::count(function->parameter_types().begin(), function->parameter_types().end(), 'k') != 1)
						{
							throw std::runtime_error("Multiple constants are not supported in ROP functions");
						}
						
						if (call_param_type.at(param_n) == 'r')
						{
							insert_call("equals", "lv", { return_storage, function->parameters().at(param_n) });	
						}
						else
						{
							insert_call("equals", { call_param_type.at(param_n), 'v' }, { call_param.at(param_n), function->parameters().at(param_n) });	
						}
						
						if (std::distance(applicable_functions.begin(), std::find(applicable_functions.begin(), applicable_functions.end(), function)) == 0)
						{
							insert_call("add", "rv", { default_value, this->constant_ref(1) });
							insert_call("and", "rv", { default_value, this->constant_ref(1) });
						}
						
						insert_call("lsl", "rv", { default_value, this->constant_ref(ctz(4*word_size)) }); // 32 bit equals: 8/0, 16 bit equals: 4/0, 8 bit equals: 2/0
					}
					
					ctr++;
				}
			}
			else
			{
				// x + (-x) = 0. maybe a user wishes for ascii armour, so 0 would be a bad value to default
				insert_call("add", "vv", { this->constant_ref(DEFAULT_VALUE), this->constant_ref(-DEFAULT_VALUE) });
			}
			
			// store path to our function structure
			auto struct_offset = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			insert_call("add", "vr", { this->additive_ref(m_local_work_ref, function_base*word_size), default_value });
			insert_call("store", "rv", { default_value, struct_offset });
			
			// save the address
			auto address_ref = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			insert_call("load", "l", { struct_offset });
			insert_call("store", "rv", { default_value, address_ref });
		
			// load our size and store in next offset
			auto size_ref = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			insert_call("add", "lv", { struct_offset, this->constant_ref(word_size) });
			insert_call("load", "r", { default_value });
			insert_call("store", "rv", { default_value, size_ref });
			
			// load our working stack index and store in next offset
			auto working_stack_index = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			insert_call("add", "lv", { struct_offset, this->constant_ref(2*word_size) });
			insert_call("load", "r", { default_value });
			insert_call("store", "rv", { default_value, working_stack_index });
			
			// load our working stack address array base and store in next offset
			auto working_stack = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			insert_call("add", "lv", { struct_offset, this->constant_ref(3*word_size) });
			insert_call("load", "r", { default_value });
			insert_call("store", "rv", { default_value, working_stack });
			
			insert_call("load", "l", { working_stack_index });
			insert_call("store", "rv", { default_value, tmp });
			
			// index is only 1 bit
			insert_call("load", "l", { working_stack_index });
			insert_call("and", "rv", { default_value, this->constant_ref(1) });
			
			auto tz = ctz(word_size);
			
			if (tz)
				insert_call("lsl", "rv", { default_value, this->constant_ref(tz) });
	
			insert_call("add", "lr", { working_stack, default_value });
			insert_call("load", "r", { default_value });
			
			// get base address for our working stack
			auto stack_base_ref = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			insert_call("sub", "rl", { default_value, size_ref });
			insert_call("store", "rv", { default_value, stack_base_ref });
			
			// update working stack index
			insert_call("load", "l", { working_stack_index });
			insert_call("add", "rv", { default_value, this->constant_ref(1) });
			insert_call("and", "rv", { default_value, this->constant_ref(1) });
			insert_call("store", "rl", { default_value, working_stack_index });
			
			// copy function to working stack
			insert_call("memcpy", "lll", { stack_base_ref, address_ref, size_ref });
			
			// store parameters in local 
			auto local_offset = work_counter;
			
			for (auto i = 0; i < call_param.size(); ++i)
			{
				// check if return
				if (call_param_type.at(i) == 'r')
				{
					insert_call("store", "lv", { return_storage, this->additive_ref(m_local_work_ref, (work_counter++)*word_size) });
				}
				else
				{
					insert_call("store", { call_param_type.at(i), 'v' }, { call_param.at(i), this->additive_ref(m_local_work_ref, (work_counter++)*word_size) });	
				}
			}
			
			// now store on param memory
			// TODO: memcpy instead?
			for (auto i = 0; i < call_param.size(); ++i)
			{
				insert_call("store", "lv", { this->additive_ref(m_local_work_ref, (local_offset+i)*word_size), this->additive_ref(m_rop_param, i*word_size) });	
			}
			//insert_call("infinite_loop", "v", {default_value});
						// reset work counter, can be invalidated by function call
						
			m_work_counter_max = std::max(m_work_counter_max, work_counter);
			work_counter = 0;
			
			auto caller_id = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			auto buffer_index = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			
			// index is only 1 bit
			insert_call("load", "l", { working_stack_index });
			insert_call("add", "rv", { default_value, this->constant_ref(1) });
			insert_call("and", "rv", { default_value, this->constant_ref(1) });
			insert_call("store", "rv", { default_value, buffer_index });
			
			// create our caller id entry
			auto id = new CallerId(rop_function, m_gen_program->data(), word_size);
			
			insert_call("store", "vv", { id->reference(), caller_id });
			
			// pivot the stack pointer to this new location
			insert_call("pivot", "l", { stack_base_ref });
			
			auto insertion_point = rop_function->calls().begin();
			std::advance(insertion_point, m_insertion_pos-1);
			id->set_pivot(insertion_point);
			m_caller_id_table.push_back(id);
	
			// reset work counter, can be invalidated by function call
			m_work_counter_max = std::max(m_work_counter_max, work_counter);
			work_counter = 0;
			
			// load return value
			auto return_value = this->additive_ref(m_local_work_ref, (work_counter++)*word_size);
			insert_call("store", "lv", { m_rop_param, return_value });
			
			if (found_uses)
			{
				// we need to reset our parameters
				insert_call("add", "lv", { m_rop_stack, this->constant_ref(0-(function_param_count*word_size)) });
				insert_call("store", "rv", { default_value, m_local_work_ref });
				
				for (auto i = 0; i < function_param_count; ++i)
				{	
					insert_call("add", "lv", { m_rop_stack, this->constant_ref(i*word_size) });
					insert_call("store", "rv", { default_value, this->additive_ref(m_rop_param, i*word_size) });
				}
			}
			
			insert_call("load", "v", { return_value });
			m_work_counter_max = std::max(m_work_counter_max, work_counter);
		};
		
		m_deferred_insertions.push_back(std::make_pair(insert_rop_call, [rop_function, this](auto const& function, auto const& types, auto const& params)
		{
			std::string call_prototype = function + std::string("(") + types + ")";

			// lookup best map
			GadgetMapPtr map = m_target->bestGadgetMap(call_prototype);
			
			// check for null
			if (!map)
			{
				// no map found!
				throw std::runtime_error("Target has no support for call scheme: " + types);
			}

			RopFunctionCallPtr function_call(new RopFunctionCall);
			
			// set function call data
			function_call->setMap(map);
			function_call->setParameters(params);
			
			// if its not a function map, we need to give it a function
			if (!map->isFunction())
			{
				// TODO: fixup
				auto ref = m_function_table.at(std::make_pair("*global", function));
				function_call->setFunction(ref);
			}
		
			auto insertion_point = rop_function->calls().begin();
			std::advance(insertion_point, m_insertion_pos++);
			
			rop_function->calls().insert(insertion_point, function_call);
		}));
	}
}

void CodeGenerator::operator()(roptool::ast::return_statement const& ast)
{
	auto namespace_ = m_namespace;
	
	ArgumentVisitor argument_visitor(m_param_type, m_param, m_gen_program, m_target, m_zero_ref, [namespace_, this](auto& id)
	{
		DataRefPtr ref;

		// if global, first check local scope
		if (id.namespace_ == "*global")
		{
			try
			{
				ref = m_reference_table.at(std::make_pair(namespace_, id.name));
				return ref;
			}
			catch (...)
			{
				// do nothing
			}
		}

		return m_reference_table.at(std::make_pair(id.namespace_, id.name));
	});

	boost::apply_visitor(argument_visitor, ast);
	
	auto word_size = m_target->manifest()->arch_bitlen()/8;
	auto default_value = constant_ref(DEFAULT_VALUE);
	unsigned int work_counter = 0;
	auto caller_id = additive_ref(m_local_work_ref, (work_counter++)*word_size);
	auto return_value = additive_ref(m_local_work_ref, (work_counter++)*word_size);
	
	// store return value
	insert_call("store", "rv", { default_value, return_value });
	
	// load return address
	insert_call("load", "l", { return_address_ref() });
	insert_call("store", "rv", { default_value, caller_id });
	
	// decrement counter
	insert_call("load", "l", { return_address_recursive_count() });
	insert_call("add", "rv", { default_value, constant_ref(-1) });
	insert_call("store", "rl", { default_value, return_address_recursive_count() });
	
	insert_call("load", "l", { return_address_recursive_count() });
	insert_call("equals", "rv", { default_value, constant_ref(0) });
	
	auto tz = ctz(word_size);
	
	if (tz)
		insert_call("lsl", "rv", { default_value, constant_ref(tz) });
	
	auto recursive_shrink = additive_ref(m_local_work_ref, (work_counter++)*word_size);
	insert_call("store", "rv", { default_value, recursive_shrink });
	insert_call("sub", "ll", { return_address_recursive_count(), recursive_shrink });
	insert_call("store", "rv", { default_value, return_address_recursive_count() });
	
	// decrement return address if needed
	insert_call("sub", "ll", { return_address_ref(), recursive_shrink });
	insert_call("store", "rv", { default_value, return_address_ref() });
	
	// store value in param slot
	if (m_param_type.at(0) == 'r')
	{
		insert_call("store", "lv", { return_value, m_rop_param });	
	}
	else
	{
		insert_call("store", { m_param_type.at(0), 'v' }, { m_param.at(0), m_rop_param });	
	}
	
	auto stack_data = additive_ref(m_local_work_ref, (work_counter++)*word_size);
	insert_call("load", "l", { caller_id });
	insert_call("store", "rv", { default_value, stack_data });
	
	auto stack_data_size = additive_ref(m_local_work_ref, (work_counter++)*word_size);
	insert_call("add", "lv", { caller_id, constant_ref(word_size) });
	insert_call("load", "r", { default_value });
	insert_call("store", "rv", { default_value, stack_data_size });
	
	auto working_stack_index_addr = additive_ref(m_local_work_ref, (work_counter++)*word_size);
	insert_call("add", "lv", { caller_id, constant_ref(3*word_size) });
	insert_call("load", "r", { default_value });
	insert_call("store", "rv", { default_value, working_stack_index_addr });
	
	auto working_stack_offset = additive_ref(m_local_work_ref, (work_counter++)*word_size);
	insert_call("load", "l", { working_stack_index_addr });
	insert_call("lsl", "rv", { default_value, constant_ref(tz) });
	insert_call("store", "rv", { default_value, working_stack_offset });
	insert_call("add", "lv", { caller_id, constant_ref(2*word_size) });
	insert_call("load", "r", { default_value });
	insert_call("add", "lr", { working_stack_offset, default_value });
	insert_call("load", "r", { default_value });
	insert_call("sub", "rl", { default_value, stack_data_size });
	insert_call("store", "rv", { default_value, working_stack_offset });
	
	// flip bit
	insert_call("load", "l", { working_stack_index_addr });
	insert_call("add", "rv", { default_value, constant_ref(1) });
	insert_call("and", "rv", { default_value, constant_ref(1) });
	insert_call("store", "rl", { default_value, working_stack_index_addr });
	
	// setup clean stack for return
	insert_call("memcpy", "lll", { working_stack_offset, stack_data, stack_data_size });

	// return to our caller
	insert_call("pivot", "l", { working_stack_offset });
	m_work_counter_max = std::max(m_work_counter_max, work_counter);
}

void CodeGenerator::operator()(roptool::ast::identifier const& ast)
{
	std::cout << "got identifier!\n";
	m_param_type.push_back('t');
}

void CodeGenerator::operator()(roptool::ast::expression const& ast)
{
	// tie the value to an expression
	ExpressionReducer expression([this](auto& id)
	{
		std::cout << "name found:" << id.namespace_ << "::" << id.name << "\n";
		return m_reference_table.at(std::make_pair(id.namespace_, id.name));
	});

	auto rhs = expression(ast);

	if (!rhs->constant())
	{
		// do we require this to be constant
		std::cout << "code parameter must be constant!\n";
	}

	m_param_type.push_back('k');
	m_param.push_back(rhs);
}

void CodeGenerator::operator()(roptool::ast::code_section const& ast)
{
	auto word_size = m_target->manifest()->arch_bitlen()/8;
	
	m_param_type.clear();
	m_param.clear();
	
    m_rop_func.reset(new RopFunction);
    m_rop_func->setName(ast.identifier);
	m_rop_function_table.insert(std::make_pair(ast.identifier, m_rop_func));
	
	for (auto const& param : ast.parameters)
	{
		std::cout << "expression: ";
		boost::apply_visitor(*this, param);
		std::cout << "\n";
	}

	m_param_type.push_back('\0');
	std::string type(m_param_type.data());

	std::cout << "got code section type: " << type << "\n";
	m_namespace = "*local_" + ast.identifier + "(" + type + ")";
	
	std::cout << "entering scope: " << m_namespace << "\n";
	
	m_variable_param_count = std::count(type.begin(), type.end(), 't');

	std::cout << "got size of: " << word_size*m_variable_param_count << "\n";
	std::cout << "data section size: " << m_gen_program->data().size() << "\n";
	
	DataRefPtrList params;
	
	{
		auto param = ast.parameters.begin();
		for (auto i = 0, c = 0; i < type.length() && param != ast.parameters.end(); ++i, ++param)
		{
			if (type.at(i) == 't' && c < m_variable_param_count)
			{
				auto identifier = boost::get<roptool::ast::identifier>((*param).get());
				auto ref = additive_ref(m_rop_param, word_size*c);

				// add to reference table
				m_reference_table[std::make_pair(m_namespace, identifier)] = ref;
				params.push_back(ref);
				c++;
			}
			else
			{
				params.push_back(m_param.at(i-c));
			}
		}	
	}
	
	std::cout << "adding rop function: (" << params.size() << "," << type.size() << ")\n";
	m_rop_func->set_parameters(params, type);
	
	if (m_namespace != "*local_entry()")
	{
		unsigned int work_counter = 0;
		auto caller_id =  additive_ref(m_local_work_ref, (work_counter++)*word_size);
		auto return_address =  additive_ref(m_local_work_ref, (work_counter++)*word_size);
		auto return_match_result = additive_ref(m_local_work_ref, (work_counter++)*word_size);
		auto temp = additive_ref(m_local_work_ref, (work_counter++)*word_size);
		
		auto default_value = constant_ref(DEFAULT_VALUE);
			
		// store return address
		insert_call("store", "rv", { default_value, return_address });
		
		// update return address list
		insert_call("load", "l", { return_address_ref() });
		insert_call("equals", "rl", { default_value, caller_id });
		insert_call("store", "rv", { default_value, return_match_result });
		insert_call("add", "lv", { return_match_result, constant_ref(1) });
		insert_call("and", "rv", { default_value, constant_ref(1) });
		insert_call("lsl", "rv", { default_value, constant_ref(2) });
		insert_call("add", "lr", { return_address_ref(), default_value });
		insert_call("store", "rv", { default_value, return_address_ref() });
		insert_call("load", "v", { return_address_ref() });
		insert_call("store", "lr", { caller_id, default_value });

		// update return address recursive count
		insert_call("add", "lv", { return_match_result, constant_ref(1) });
		insert_call("and", "rv", { default_value, constant_ref(1) });
		
		auto tz = ctz(word_size);

		if (tz)
			insert_call("lsl", "rv", { default_value, constant_ref(tz) }); // *1 for 8 bit, *2 for 16 bit, *4 for 32 bit

		// update pointer, + word_size if no match, otherwise + 0
		insert_call("add", "lr", { return_address_recursive_count(), default_value });
		insert_call("store", "rv", { default_value, return_address_recursive_count() });

		// if equals, number & 0xFFFF... +1 else number & 0 + 1
		insert_call("add", "lv", { return_match_result, constant_ref(1) });
		insert_call("and", "rv", { default_value, constant_ref(1) });
		insert_call("add", "rv", { default_value, constant_ref(-1) }); // equals = -1, not equal = 0
		insert_call("store", "rv", { default_value, temp });
		insert_call("load", "l", { return_address_recursive_count() });
		insert_call("and", "rl", { default_value, temp });
		insert_call("add", "rv", { default_value, constant_ref(1) });
		insert_call("store", "rl", { default_value, return_address_recursive_count() });

		m_work_counter_max = std::max(m_work_counter_max, work_counter);
		work_counter--;

		// TODO: top levelchecks for uses of parameters beyond this call
		auto found_uses = false;

		if (found_uses)
		{
			// increment and decrement of parameter base required
			insert_call("load", "l", { m_rop_stack });
			insert_call("store", "rv", { default_value, m_local_work_ref });
			insert_call("add", "lv", { m_rop_stack, constant_ref(m_variable_param_count*word_size) });
			insert_call("store", "rv", { default_value, m_rop_stack });

			for (auto i = 0; i < m_variable_param_count; ++i)
			{
				insert_call("load", "l", { m_rop_stack });
				insert_call("add", "rv", { default_value, constant_ref(i*word_size) });
				insert_call("store", "lr", { additive_ref(m_rop_param, i*word_size), default_value });
			}
		}
		else
		{
			insert_call("store", "vv", { m_rop_param, m_local_work_ref });
		}
		auto is_test = m_rop_func->name() == "asdf";
			
			
		if (is_test) insert_call("infinite_loop", "", {});
	}
	
	for (auto const& statement : ast.statements)
	{
		m_param_type.clear();
		m_param.clear();

		boost::apply_visitor(*this, statement);
	}
	
	m_gen_program->code().add(m_rop_func);
}

DataRefPtr CodeGenerator::return_address_ref(void)
{
	auto word_size = m_target->manifest()->arch_bitlen()/8;
	
	if (!m_return_address_ref)
	{
		// TODO: make configurable
		m_gen_program->data().add(std::string("return address buffer"));
		auto return_address_buffer = m_gen_program->data().deferred_buffer(0x5000);
		m_return_address_ref = m_gen_program->data().add(return_address_buffer, word_size);
	}
	
	return m_return_address_ref;
}

DataRefPtr CodeGenerator::return_address_recursive_count(void)
{
	auto word_size = m_target->manifest()->arch_bitlen()/8;
	
	if (!m_return_address_recursive_count)
	{
		// TODO: make configurable
		m_gen_program->data().add(std::string("return_address_recursive_count_buffer"));
		auto return_address_recursive_count_buffer = m_gen_program->data().deferred_buffer(0x5000);
		m_gen_program->data().add(std::string("return_address_recursive_count number"));
		m_return_address_recursive_count = m_gen_program->data().add(return_address_recursive_count_buffer, word_size);
	}
	
	return m_return_address_recursive_count;
}

ProgramPtr CodeGenerator::compile(const roptool::ast::ropscript& ast, TargetPtr target)
{
	m_work_counter_max = 0;
	auto word_size = target->manifest()->arch_bitlen()/8;
	
    // create new program
    m_gen_program.reset(new Program);
    m_gen_program->setTarget(target);
    
	m_local_work_ref = std::make_shared<DataReference>();
	m_rop_param = std::make_shared<DataReference>();
	
    // create a zero ref
    m_zero_ref = constant_ref(0);
    m_target = target;

	for (const auto& aslr : m_target->aslr_table())
	{
		m_reference_table[std::make_pair("ASLR", aslr.first)] = aslr.second;
	}
	
	for (auto const& oper : ast)
		boost::apply_visitor(*this, oper);
	
	if (!m_deferred_insertions.empty())
	{
		// TODO: make configurable
		m_gen_program->data().add(std::string("rop stack"));
		auto rop_stack_buffer = m_gen_program->data().deferred_buffer(0x1000);
		m_rop_stack = m_gen_program->data().add(rop_stack_buffer, word_size);

	
		for (auto& deferred_insertion : boost::adaptors::reverse(m_deferred_insertions))
		{
			deferred_insertion.first(deferred_insertion.second);
		}
		
		if (m_work_counter_max)
		{
			m_gen_program->data().add(std::string("local work buffer"));
			auto ref = m_gen_program->data().deferred_buffer(m_work_counter_max*word_size);
            m_local_work_ref->set(ref);
		}
	
		// set to 1 as we always need at least 1 slot for return values
		size_t max_param = 1;
		
		// create our working buffers
		auto stack_reserve = 0x400;
		
		for (auto& function : m_gen_program->code().functions())
		{
			if (function->data_reference_used())
			{
				max_param = std::max(max_param, function->parameters().size());
				
				// create working buffer
				auto size = function->size(m_target) + stack_reserve;
				
				// create index working buffer
				m_gen_program->data().add(std::string("") + function->name() + " - work buffer index");
				auto index_ref = m_gen_program->data().deferred_buffer(word_size);
				auto function_index_ref = function->working_stack_index();
				
                function_index_ref->set(index_ref);
				
				// create two buffers
				m_gen_program->data().add(std::string("") + function->name() + " - work buffer 1");
				auto buffer1 = m_gen_program->data().deferred_buffer(size);
				m_gen_program->data().add(std::string("") + function->name() + " - work buffer 2");
				auto buffer2 = m_gen_program->data().deferred_buffer(size);	
				DataRefPtrList working_stacks = { buffer1, buffer2 };
				
				// create a location to hold these values
				auto working_array = m_gen_program->data().add(working_stacks, word_size, [size](auto dataref)
				{
					auto ref = std::make_shared<DataReference>(dataref->get()+size);
                    ref->set(dataref, size, DataReferenceOperation::ADDITION);
					return ref;
				});
				
				auto function_buffer_ref = function->working_stack();
                function_buffer_ref->set(working_array);
			}
		}
		
		m_gen_program->data().add(std::string("rop params"));
		auto ref = m_gen_program->data().deferred_buffer(max_param*word_size);
		
		m_rop_param->set(ref);
		
		for (auto caller_id : m_caller_id_table)
		{
			caller_id->resolve(m_target);
		}
	}
	
	return m_gen_program;
}
