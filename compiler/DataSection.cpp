// roptool
#include "DataSection.h"

// boost
#include <boost/iterator/filter_iterator.hpp>

// std
#include <algorithm>
#include <cstring>

namespace
{
	// TODO: big endian
	void int_copy(void *dst, u64 src, u64 len)
	{
		for (auto i = 0; i < len; ++i)
		{
			((u8 *)dst)[i] = (src & (0xFF << i*8)) >> i*8;
		}
	}
}

void DataSection::setBase(u64 base_address)
{
    m_base_address = base_address;
	m_base_addr->set(base_address);
}

void DataSection::set_alignment(int align)
{
	m_padding = align;
}

DataRefPtr DataSection::add(u64 constant)
{
	auto ref = std::make_shared<DataReference>(constant, true);
    return ref;
}

DataRefPtr DataSection::add(const std::string& str)
{
    return add(str.data(), str.length()+1);
}

DataRefPtr DataSection::add(const void *data, u64 size)
{
    // get current position
    u64 offset = m_data.size();
	u64 padding = m_padding - (offset % m_padding);
	
	// resize then copy memory
	m_data.resize(offset+size+padding);
	std::memset(m_data.data()+offset, 0, padding);
	std::memcpy(m_data.data()+offset+padding, data, size);
    
    auto ref = std::make_shared<DataReference>(m_base_addr->get()+offset+padding);
    ref->set(m_base_addr, offset + padding, DataReferenceOperation::ADDITION);
    return ref;
}

DataRefPtr DataSection::add(DataRefPtr data, u64 size)
{
    // get current position
    u64 offset = m_data.size();
	u64 padding = m_padding - (offset % m_padding);
	
	// resize then copy memory
	m_data.resize(offset+size+padding);
	std::memset(m_data.data()+offset, 0, padding);

	if (data->constant())
	{
		// no need for update
		int_copy(m_data.data()+offset+padding, data->get(), size);
	}
	else
	{
		DynamicDataReference ref;
		
		ref.offset = offset+padding;
		ref.size = size;
		ref.ref = data;
		
		m_dynamic_data_references.push_back(ref);
	}
	
    auto ref = std::make_shared<DataReference>(m_base_addr->get()+offset+padding);
    ref->set(m_base_addr, offset + padding, DataReferenceOperation::ADDITION);
    return ref;
}

DataRefPtr DataSection::add(DataRefPtrList& refs, u64 size)
{
	return add(refs, size, [](auto ref)
	{
		return ref;
	});
}

DataRefPtr DataSection::add(DataRefPtrList& refs, u64 size, ReferenceModifier reference)
{
    // get current position
    u64 offset = m_data.size();
	u64 padding = m_padding - (offset % m_padding);
	
	// resize then copy memory
	m_data.resize(offset+refs.size()*size+padding);
	std::memset(m_data.data()+offset, 0, padding);

	offset += padding;
	
	auto i = 0;
	
	for (auto& data : refs)
	{
		if (data->constant())
		{
			// no need for update
			int_copy(m_data.data()+offset+i*size, data->get(), size);
			continue;
		}
		
		DynamicDataReference ref;
		
		ref.offset = offset+i*size;
		ref.size = size;
		ref.ref = reference(data);
		
		m_dynamic_data_references.push_back(ref);
	}
	
    auto ref = std::make_shared<DataReference>(m_base_addr->get()+offset);
    ref->set(m_base_addr, offset, DataReferenceOperation::ADDITION);
    return ref;	
}

DataRefPtr DataSection::deferred_buffer(u64 size)
{
	// get current position
    u64 offset = m_data.size();
	u64 padding = m_padding - (offset % m_padding);

	// resize then initialise to default
	// TODO: make default configurable
	m_data.resize(offset+size+padding);
	std::memset(m_data.data()+offset, 0, padding);
	
	offset += padding;
	
	std::memset(m_data.data()+offset, 0, size);
	
    auto ref = std::make_shared<DataReference>(m_base_addr->get()+offset);
    ref->set(m_base_addr, offset, DataReferenceOperation::ADDITION);
	return ref;
}

bool DataSection::update(DataRefPtr ref, const void *data, u64 size)
{
    if (ref->constant())
		return false;

	// copy memory
	std::memcpy(m_data.data()+(ref->get()-m_base_addr->get()), data, size);
    return true;
}

u64 DataSection::size(void)
{
    return m_data.size();
}

u64 DataSection::base(void)
{
    return m_base_address;
}

const std::vector<u8>& DataSection::data(void)
{
	for (const auto& ref : m_dynamic_data_references)
	{
		int_copy(m_data.data()+ref.offset, ref.ref->get(), ref.size);
	}
	
    return m_data;
}

RelocationList DataSection::relocations(TargetPtr target)
{
    RelocationList list;
    
    if (!target->manifest()->aslr())
        return list;
    
    auto aslr_table = target->aslr_table(); 
    
	for (const auto& ref : m_dynamic_data_references)
	{
        if (ref.ref->constant())
            continue;
        
        auto relocs = create_relocation_for_aslr(aslr_table, ref.ref);

        for (auto& reloc : relocs)
        {
            reloc.offset = std::make_shared<DataReference>();
            reloc.offset->set(m_data_base, ref.offset, DataReferenceOperation::ADDITION);
        }

        list.insert(list.end(), relocs.begin(), relocs.end());
    }
    
    return list;
}

DataRefPtr DataSection::data_section_base(void)
{
    return m_data_base;
}

DataRefPtr DataSection::base_address(void)
{
    return m_base_addr;
}

DataSection::DataSection(void)
	: m_base_addr(new DataReference)
    , m_data_base(new DataReference)
	, m_padding(1)
{
}
