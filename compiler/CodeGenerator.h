#ifndef _CODE_GENERATOR_H_
#define _CODE_GENERATOR_H_

#include "Program.h"
#include "Target.h"

#include <parser/ast.h>

// std
#include <memory>
#include <string>

class CallerId;

class CodeGenerator
{
    public:
        // CTOR
        CodeGenerator(void) = default;
        
        // compile
        ProgramPtr compile(const roptool::ast::ropscript& ast, TargetPtr m_target);

	public:
		void operator()(roptool::ast::identifier const& ast);
		void operator()(roptool::ast::expression const& ast);
		void operator()(roptool::ast::symbol_decl const& ast);
		void operator()(roptool::ast::functiondef_decl const& ast);
		void operator()(roptool::ast::buffer_decl const& ast);
		void operator()(roptool::ast::variable_decl const& ast);
		void operator()(roptool::ast::data_section const& ast);
		void operator()(roptool::ast::function_call const& ast);
		void operator()(roptool::ast::return_statement const& ast);
		void operator()(roptool::ast::code_section const& ast);
		
	private:
		void insert_call(const std::string& function, const std::string& types, const DataRefPtrList& list);
		DataRefPtr constant_ref(u64 constant);
		DataRefPtr additive_ref(DataRefPtr base, u64 constant);
		DataRefPtr return_address_ref(void);
		DataRefPtr return_address_recursive_count(void);
		
    private:
		using NamespaceIdentifier = std::pair<std::string, std::string>;

        u64 bit_mask(int bits);
        
        TargetPtr m_target;
        
        std::map<std::string, Function> m_functions;
        
        // data section
        DataRefPtr m_zero_ref;
        DataRefPtr m_local_work_ref, m_return_address_ref, m_return_address_recursive_count, m_rop_stack, m_rop_param;
		unsigned int m_work_counter_max{0}, m_rop_param_max{0};
		
		u64 m_variable_param_count;
		
		size_t m_insertion_pos;
		
        // rop section
        RopFunctionPtr m_rop_func;
        
		using InsertCall = std::function<void(const std::string&, const std::string&, const DataRefPtrList&)>;
		std::vector<std::pair<std::function<void(InsertCall)>, InsertCall>> m_deferred_insertions;
		
		// the generated program
        ProgramPtr m_gen_program;
        
        // this stores the function call param types
        std::vector<char> m_param_type;
        
		std::list<CallerId*> m_caller_id_table;
		
        // this stores the function call data references
        DataRefPtrList m_param;

		std::map<std::string, std::pair<DataRefPtr, DataRefPtr>> m_working_stack_table;
		std::map<NamespaceIdentifier, DataRefPtr> m_reference_table;
		std::map<NamespaceIdentifier, DataRefPtr> m_function_table;
		std::multimap<std::string, RopFunctionPtr> m_rop_function_table;
		std::map<u64, DataRefPtr> m_constants;
		std::string m_namespace;
};

#endif // _CODE_GENERATOR_H_
