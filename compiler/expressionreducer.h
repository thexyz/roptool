#ifndef EXPRESSIONREDUCER_H
#define EXPRESSIONREDUCER_H

#include "DataRef.h"

#include <parser/ast.h>

#include <functional>

class DataSection;

class ExpressionReducer
{
public:
	using IdentifierLookup = std::function<DataRefPtr(roptool::ast::namespace_identifier const&)>;

public:
	ExpressionReducer(IdentifierLookup id_lookup)
		: m_id_lookup(id_lookup)
	{
	}

	DataRefPtr operator()(roptool::ast::u64 const& ast) const;
	DataRefPtr operator()(roptool::ast::u32 const& ast) const;
	DataRefPtr operator()(roptool::ast::u16 const& ast) const;
	DataRefPtr operator()(roptool::ast::u8 const& ast) const;
	DataRefPtr operator()(roptool::ast::constant const& ast) const;
	DataRefPtr operator()(roptool::ast::namespace_identifier const& ast) const;
	DataRefPtr operator()(DataRefPtr lhs, roptool::ast::operation const& ast) const;
	DataRefPtr operator()(roptool::ast::unary_operand const& ast) const;
	DataRefPtr operator()(roptool::ast::expression const& ast) const;

private:
	IdentifierLookup m_id_lookup;
};

#endif // EXPRESSIONREDUCER_H
