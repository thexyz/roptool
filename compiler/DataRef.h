#ifndef DATAREFERENCE_H
#define DATAREFERENCE_H

// roptool
#include "Types.h"

// std
#include <functional>
#include <list>
#include <vector>
#include <memory>
#include <map>

// boost
#include <boost/graph/adjacency_list.hpp>
#include <boost/variant.hpp>

// forward declare classes
class DataReference;

// define smart pointer types
typedef std::shared_ptr<DataReference> DataRefPtr;
typedef std::vector<DataRefPtr> DataRefPtrList;

enum class DataReferenceOperation
{
	ADDITION,
	SUBTRACTION,
	MULTIPLICATION,
	DIVISION
};

enum class DataReferenceUnaryOperation
{
	NEGATIVE,
	ONES_COMPLEMENT,
    EQUAL
};

class DataReference
{
public:
    using OperationType = boost::variant<DataReferenceOperation, DataReferenceUnaryOperation>;
    
    using Vertex = struct
    {
        DataReference *ref;
        OperationType op;
        bool own;
    };
    
    enum OperandPosition
    {
        LEFT,
        RIGHT
    };
    
    using Edge = struct
    {
        OperandPosition side;
    };
    
    using Graph = boost::adjacency_list<boost::listS, boost::vecS, boost::bidirectionalS, Vertex, Edge>;
    using Operand = boost::variant<u64, DataRefPtr>;
    using GraphTraits = boost::graph_traits<DataReference::Graph>;
    using VertexDescriptor = GraphTraits::vertex_descriptor;
    
public:
	DataReference(u64 init = 0, bool constant = false);
	virtual ~DataReference(void) = default;

	u64 get(void) const;
    void set(Operand rhs, DataReferenceUnaryOperation op = DataReferenceUnaryOperation::EQUAL);
	void set(Operand op1, Operand op2, DataReferenceOperation op);
    
	bool constant(void) const;
    Graph *to_graph(void);
    
private:
    void recalculate(void);
	void set_constant(void);
    void on_update(DataReference *ref);
	void add_tree(Graph *g, DataReference *ref, boost::graph_traits<Graph>::vertex_descriptor vertex);
    
private:
	u64 m_value;
	bool m_constant;
    OperationType m_op;
    DataRefPtr m_operand;
    DataRefPtr m_operand2;
    std::list<DataReference*> m_subscribers;
	std::map<DataReferenceOperation, std::function<u64(u64, u64)>> m_operation_handlers;
	std::map<DataReferenceUnaryOperation, std::function<u64(u64)>> m_unary_operation_handlers;
};

#endif // DATAREFERENCE_H
