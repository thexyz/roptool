#include "relocation.h"

#include <boost/iterator/filter_iterator.hpp>
#include <iostream>

namespace {
void analyse_precedence(DataReference::OperationType op_, bool& is_additive, bool& is_multiplicative, bool& is_unary)
{
    is_additive = false;
    is_multiplicative = false;
    is_unary = false;
    
    switch (op_.which())
    {
        case 0: // DataReferenceOperation
        {
            auto op = boost::get<DataReferenceOperation>(op_);
            
            switch (op)
            {
                case DataReferenceOperation::ADDITION:
                case DataReferenceOperation::SUBTRACTION:
                    is_additive = true;
                    break;
                case DataReferenceOperation::MULTIPLICATION:
                case DataReferenceOperation::DIVISION:
                    is_multiplicative = true;
                    break;
            }
            break;
        }
        
        case 1: // DataReferenceUnaryOperation
        {
            is_unary = true;
            
            std::cout << "UNARYT!!!\n";
            break;
        }
        
        default: // unknown
        {
            BOOST_ASSERT(0);
            break;
        }                  
    }
}

bool shared_precedence(DataReference::OperationType op1, DataReference::OperationType op2)
{
    bool op1_additive, op1_multiplicative, op1_unary;
    bool op2_additive, op2_multiplicative, op2_unary;
    
    analyse_precedence(op1, op1_additive, op1_multiplicative, op1_unary);
    analyse_precedence(op2, op2_additive, op2_multiplicative, op2_unary);
    
    if (op1_additive && op2_additive)
        return true;
    
    if (op1_multiplicative && op2_multiplicative)
        return true;
    
    // TODO: unary...
    return false;
}

bool is_left_node(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor node, DataReference::Graph *graph)
{
    auto e = boost::edge(parent, node, *graph);
    BOOST_ASSERT(e.second);
    return (*graph)[e.first].side == DataReference::LEFT;
}

bool is_operation(DataReference::OperationType op, DataReference::OperationType op2)
{
    return op == op2;
}

void move_to_left(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor node, DataReference::Graph *graph)
{
    auto e = boost::edge(parent, node, *graph);
    BOOST_ASSERT(e.second);
    (*graph)[e.first].side = DataReference::LEFT;
}

DataReference::VertexDescriptor get_sibling(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor node, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    
    OutEdgeIterator out_i, out_end;
    for (boost::tie(out_i, out_end) = boost::out_edges(parent, *graph); out_i != out_end; ++out_i)
    {
        auto child = boost::target(*out_i, *graph);
        
        if (child != node)
        {
            return child;
        }
    }
    
    BOOST_ASSERT(0);
    while (1);
}

bool promote_constant(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor vertex, DataReference::VertexDescriptor constant, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    
    std::cout << "optimising: (" << (u64)parent << ", " << (u64)vertex << ", " << (u64)constant << ")\n";
    
    // check that both types can interact
    if (!shared_precedence((*graph)[vertex].op, (*graph)[parent].op))
        return false;
    
    // add constant to parent if possible
    OutEdgeIterator out_i, out_end;
    for (boost::tie(out_i, out_end) = boost::out_edges(parent, *graph); out_i != out_end; ++out_i)
    {
        auto child = boost::target(*out_i, *graph);
        
        if ((*graph)[child].ref->constant())
        {
            auto child_lhs = is_left_node(vertex, constant, graph);
            auto parent_lhs = is_left_node(parent, child, graph);
            
            auto child_lhs_ref = std::make_shared<DataReference>(child_lhs ? (*graph)[constant].ref->get() : 0, true);
            auto child_rhs_ref = std::make_shared<DataReference>(!child_lhs ? (*graph)[constant].ref->get() : 0, true);
            auto child_eqn = std::make_shared<DataReference>();
            child_eqn->set(child_lhs_ref, child_rhs_ref, boost::get<DataReferenceOperation>((*graph)[vertex].op));
            
            auto parent_lhs_ref = parent_lhs ? std::make_shared<DataReference>((*graph)[child].ref->get(), true) : child_eqn;
            auto parent_rhs_ref = !parent_lhs ? std::make_shared<DataReference>((*graph)[child].ref->get(), true) : child_eqn;
            auto eqn = std::make_shared<DataReference>();
            eqn->set(parent_lhs_ref, parent_rhs_ref, boost::get<DataReferenceOperation>((*graph)[parent].op));

            if ((*graph)[child].own)
                delete (*graph)[child].ref;
            
            (*graph)[child].ref = new DataReference(eqn->get(), true);
            (*graph)[child].own = true;
         
            if (is_operation((*graph)[vertex].op, DataReferenceOperation::SUBTRACTION))
            {
                (*graph)[vertex].op = DataReferenceUnaryOperation::NEGATIVE;
            }
            else
            {
                (*graph)[vertex].op = DataReferenceUnaryOperation::EQUAL;
            }
            
            if (child_lhs)
            {
                move_to_left(vertex, get_sibling(vertex, constant, graph), graph);
            }
            
            boost::remove_edge(vertex, constant, *graph);
            boost::remove_vertex(constant, *graph);
            std::cout << "got constant in parent where subtree can be promoted into\n";
            return true;
        }
    }
    
    return false;
}

bool optimise_unary(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor vertex, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    
    // this must be DataReferenceUnaryOperation
    BOOST_ASSERT((*graph)[vertex].op.which() == 1);
    
    auto op = boost::get<DataReferenceUnaryOperation>((*graph)[vertex].op);
    
    switch (op)
    {
        case DataReferenceUnaryOperation::NEGATIVE:
            if (is_operation((*graph)[parent].op, DataReferenceOperation::ADDITION))
            {
                // ensure other paremeter is on left
                if (is_left_node(parent, vertex, graph))
                {
                    move_to_left(parent, get_sibling(parent, vertex, graph), graph);
                }
                
                OutEdgeIterator out_i, out_end;
                boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
                BOOST_ASSERT(std::distance(out_i, out_end) == 1);
                
                auto value = boost::target(*out_i, *graph);
                auto e_old = boost::edge(parent, vertex, *graph);
                BOOST_ASSERT(e_old.second);
                
                auto e = boost::add_edge(parent, value, *graph);
                (*graph)[e.first].side = DataReference::RIGHT;
                
                boost::remove_edge(parent, vertex, *graph);
                boost::remove_edge(vertex, value, *graph);
                boost::remove_vertex(vertex, *graph);
                
                // change parent from addition to subtraction
                (*graph)[parent].op = DataReferenceOperation::SUBTRACTION;
                return true;
            }
            else if (is_operation((*graph)[parent].op, DataReferenceOperation::SUBTRACTION))
            {
                // if we're on the left, we need other side to be constant and negate it
                auto lhs = is_left_node(parent, vertex, graph);
                
                if (lhs)
                {
                    auto sibling = get_sibling(parent, vertex, graph);
                    
                    if (!(*graph)[sibling].ref->constant())
                    {
                        break;
                    }
                    
                    auto neg_constant = new DataReference(0-(*graph)[sibling].ref->get(), true);
                    
                    if ((*graph)[sibling].own)
                        delete (*graph)[sibling].ref;
                    
                    (*graph)[sibling].own = true;
                    (*graph)[sibling].ref = neg_constant;
                    
                    move_to_left(parent, sibling, graph);
                }
                
                // change to addition
                OutEdgeIterator out_i, out_end;
                boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
                BOOST_ASSERT(std::distance(out_i, out_end) == 1);
                
                auto value = boost::target(*out_i, *graph);
                auto e_old = boost::edge(parent, vertex, *graph);
                BOOST_ASSERT(e_old.second);
                
                auto e = boost::add_edge(parent, value, *graph);
                (*graph)[e.first].side = DataReference::RIGHT;
                
                boost::remove_edge(parent, vertex, *graph);
                boost::remove_edge(vertex, value, *graph);
                boost::remove_vertex(vertex, *graph);
                
                // change parent from subtraction to addition
                if (!lhs)
                    (*graph)[parent].op = DataReferenceOperation::ADDITION;
                return true;               
            }
            else
            {
                break;
            }
            
        case DataReferenceUnaryOperation::ONES_COMPLEMENT:
            std::cout << "no optimisation for ones complement\n";
            BOOST_ASSERT(0);
            break;
        case DataReferenceUnaryOperation::EQUAL:
        {
            OutEdgeIterator out_i, out_end;
            boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
            BOOST_ASSERT(std::distance(out_i, out_end) == 1);
            
            auto value = boost::target(*out_i, *graph);
            auto e_old = boost::edge(parent, vertex, *graph);
            BOOST_ASSERT(e_old.second);
            
            auto e = boost::add_edge(parent, value, *graph);
            (*graph)[e.first].side = (*graph)[e_old.first].side;
            
            boost::remove_edge(parent, vertex, *graph);
            boost::remove_edge(vertex, value, *graph);
            boost::remove_vertex(vertex, *graph);
            return true;
        }
        default:
            BOOST_ASSERT(0);
            break;
    }
    
    return false;
}

bool optimise_subtree(DataReference::VertexDescriptor parent, DataReference::VertexDescriptor vertex, DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    std::cout << "optimising: (" << (u64)parent << ", " << (u64)vertex << ")\n";
    auto optimised = false;
    
    // optimise all children of vertex
    OutEdgeIterator out_i, out_end;
    for (boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph); out_i != out_end;)
    {
        auto child = boost::target(*out_i, *graph);
        
        if ((*graph)[child].ref->constant())
        {
            std::cout << "got constant\n";
            if (promote_constant(parent, vertex, child, graph))
            {
                boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
                optimised = true;
            }
            else
            {
                
                ++out_i;
            }
        }
        else if ((*graph)[child].op.which() == 1)
        {
            // UNARY
            std::cout << "GOT UNARY OPTIMISATION TASK\n";
            if (optimise_unary(vertex, child, graph))
            {
                boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
                optimised = true;
            }
            else
            {
                
                ++out_i;
            }
        }
        else
        { 
            std::cout << "got subtree\n";
            if (optimise_subtree(vertex, child, graph))
            {
                boost::tie(out_i, out_end) = boost::out_edges(vertex, *graph);
                optimised = true;
            }
            else
            {
                ++out_i;
            }
        }
    }
    
    return optimised;
}

bool optimise_graph(DataReference::Graph *graph)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    auto optimised = false;
    
    if ((*graph).vertex_set().empty())
        return false;
    
    auto root = (*graph).vertex_set()[0];
    
    if ((*graph)[root].ref->constant())
        return false;
    
    // optimise all children of root
    OutEdgeIterator out_i, out_end;
    for (boost::tie(out_i, out_end) = boost::out_edges(root, *graph); out_i != out_end;)
    {
        auto child = boost::target(*out_i, *graph);
        
        if ((*graph)[child].ref->constant())
        {
            ++out_i;
            continue;
        }
        else if ((*graph)[child].op.which() == 1)
        {
            // UNARY
            std::cout << "GOT UNARY OPTIMISATION TASK\n";
            if (optimise_unary(root, child, graph))
            {
                boost::tie(out_i, out_end) = boost::out_edges(root, *graph);
                optimised = true;
            }
            else
            {
                
                ++out_i;
            }
        }        
        if (optimise_subtree(root, child, graph))
        {
            boost::tie(out_i, out_end) = boost::out_edges(root, *graph);
            optimised = true;
        }
        else
        {
            ++out_i;
        }
    }
    
    return optimised;
}

bool has_children(DataReference::VertexDescriptor node, DataReference::Graph *graph)
{    
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    
    OutEdgeIterator out_i, out_end;
    boost::tie(out_i, out_end) = boost::out_edges(node, *graph);
    return std::distance(out_i, out_end) != 0;
}

RelocationType op_to_reloc(DataReference::OperationType op, bool lhs)
{
    if (is_operation(op, DataReferenceOperation::ADDITION))
    {
        return RelocationType::ADD_SYMBOL;
    }
        
    if (is_operation(op, DataReferenceOperation::SUBTRACTION))
    {
        return lhs ? RelocationType::SUB_LHS_SYMBOL : RelocationType::SUB_RHS_SYMBOL;
    }
    
    if (is_operation(op, DataReferenceUnaryOperation::EQUAL))
    {
        return RelocationType::STORE_SYMBOL;
    }
    
    if (is_operation(op, DataReferenceUnaryOperation::NEGATIVE))
    {
        return RelocationType::SUB_RHS_SYMBOL;
    }

    BOOST_ASSERT(0);
    while(1);
}

RelocationList generate_relocs(DataReference::VertexDescriptor node, DataReference::Graph *graph, RelocationList& relocs)
{
    using OutEdgeIterator = DataReference::GraphTraits::out_edge_iterator;
    OutEdgeIterator out_i, out_end;
    boost::tie(out_i, out_end) = boost::out_edges(node, *graph);
    
    switch (std::distance(out_i, out_end))
    {
        case 1:
        {
            auto lhs = boost::target(*out_i, *graph);
            
            if (!has_children(lhs, graph))
            {
                if ((*graph)[lhs].ref->constant())
                {
                    BOOST_ASSERT(0);
                }
                
                Relocation reloc;
                reloc.type = op_to_reloc((*graph)[node].op, true);
                reloc.constant = 0;
                reloc.aslr_ref = (*graph)[lhs].ref;
                relocs.push_back(reloc);
            }
            else
            {
                generate_relocs(lhs, graph, relocs);
            }
            
            break;
        }
        case 2:
        {
            auto lhs = boost::target(*out_i, *graph);
            auto rhs = boost::target(*std::next(out_i), *graph);
            
            if (!has_children(lhs, graph) && !has_children(rhs, graph))
            {
                if ((*graph)[lhs].ref->constant() && (*graph)[rhs].ref->constant())
                {
                    BOOST_ASSERT(0);
                }
                
                else if ((*graph)[lhs].ref->constant())
                {
                    Relocation reloc;
                    
                    reloc.type = op_to_reloc((*graph)[node].op, false);
                    reloc.constant = (*graph)[lhs].ref->get();
                    reloc.aslr_ref = (*graph)[rhs].ref;
                    relocs.push_back(reloc);
                }
                
                else if ((*graph)[rhs].ref->constant())
                {
                    Relocation reloc;
                    
                    reloc.type = op_to_reloc((*graph)[node].op, true);
                    reloc.constant = (*graph)[rhs].ref->get();
                    reloc.aslr_ref = (*graph)[lhs].ref;
                    relocs.push_back(reloc);
                }
                else
                {
                    Relocation reloc;
                    
                    reloc.type = op_to_reloc((*graph)[node].op, false);
                    reloc.constant = 0;
                    reloc.aslr_ref = (*graph)[rhs].ref;
                    relocs.push_back(reloc);
                    
                    reloc.type = op_to_reloc((*graph)[node].op, true);
                    reloc.constant = 0;
                    reloc.aslr_ref = (*graph)[lhs].ref;
                    relocs.push_back(reloc);
                }
            }
            
            else if (has_children(lhs, graph))
                generate_relocs(lhs, graph, relocs);
            
            else if (has_children(rhs, graph))
                generate_relocs(rhs, graph, relocs);
            
            break;
        }
        default:
            BOOST_ASSERT(0);
            break;
    }
    
    return relocs;
}

RelocationList generate_relocs(DataReference::Graph *graph)
{
    BOOST_ASSERT(!(*graph).vertex_set().empty());  
    auto root = (*graph).vertex_set()[0];
    RelocationList relocs;
    return generate_relocs(root, graph, relocs);
}
} // anonymous

RelocationList create_relocation_for_aslr(AslrTable& aslr_table, DataReference *ref)
{
    // generate reloc information
    auto graph = ref->to_graph();
    
    auto filter = [&aslr_table, graph](const auto& ref)
    {
        for (const auto& entry : aslr_table)
        {
            if (entry.second.get() == (*graph)[ref].ref)
            {
                return true;
            }
        }
    
        return false;
    };
    
    auto begina = boost::make_filter_iterator(filter, graph->vertex_set().begin(), graph->vertex_set().end());
    auto enda = boost::make_filter_iterator(filter, graph->vertex_set().end(), graph->vertex_set().end());
    
    std::vector<boost::graph_traits<DataReference::Graph>::vertex_descriptor> aslr_verticesa(std::distance(begina, enda));
    std::copy(begina, enda, aslr_verticesa.begin());
    std::for_each(aslr_verticesa.begin(), aslr_verticesa.end(), [graph](auto ref)
    {
        std::cout << "found ASLR b4 types: " << (u64)(*graph)[ref].ref << "\n";
    });
    optimise_graph(graph);
    
    auto begin = boost::make_filter_iterator(filter, graph->vertex_set().begin(), graph->vertex_set().end());
    auto end = boost::make_filter_iterator(filter, graph->vertex_set().end(), graph->vertex_set().end());
    std::vector<boost::graph_traits<DataReference::Graph>::vertex_descriptor> aslr_vertices(std::distance(begin, end));
    std::copy(begin, end, aslr_vertices.begin());
    std::for_each(aslr_vertices.begin(), aslr_vertices.end(), [graph](auto ref)
    {
        std::cout << "found ASLR b5 types: " << (u64)(*graph)[ref].ref << "\n";
    });
    
    std::cout << "ASLR graph: vertices after: " << graph->vertex_set().size() << "\n";
    
    auto relocs = generate_relocs(graph);
    
    delete graph;
    return relocs;
}

RelocationList create_relocation_for_aslr(AslrTable& aslr_table, DataRefPtr ref)
{
    return create_relocation_for_aslr(aslr_table, ref.get());
}