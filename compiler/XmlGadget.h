#ifndef XMLGADGET_H
#define XMLGADGET_H

// roptool
#include "Gadget.h"
#include "XmlActionVisitor.h"

// std
#include <memory>
#include <string>

class XmlGadget : public Gadget
{
public:
	XmlGadget(void);
	virtual ~XmlGadget(void) = default;
	
	bool parse(const std::string& file) override;
	
	DataRefPtr address(void) const override;
	const std::string& name(void) const override;
	
protected:
	XmlActionVisitor *visitor(void);
	virtual void on_parse_complete(void);
	
private:
	std::unique_ptr<XmlActionVisitor> m_visitor;
	
	std::string m_name;
	DataRefPtr m_address;
};

typedef std::shared_ptr<XmlGadget> XmlGadgetPtr;

#endif // XMLGADGET_H
