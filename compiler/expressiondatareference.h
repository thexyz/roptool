#ifndef EXPRESSIONDATAREFERENCE_H
#define EXPRESSIONDATAREFERENCE_H

#include "expressionreducer.h"
#include "DataRef.h"

#include <parser/ast.h>

#include <set>

struct NamespaceIdentifierCompare
{
    bool operator()(const roptool::ast::namespace_identifier& lhs, const roptool::ast::namespace_identifier& rhs) const
    {
        return std::string(lhs.namespace_+"::"+lhs.name) < std::string(rhs.namespace_+"::"+rhs.name);
    }
};

struct ExpressionIdentifierCrawler
{
	using IdentifierList = std::set<roptool::ast::namespace_identifier, NamespaceIdentifierCompare>;

	void operator()(roptool::ast::constant const& ast) { }
	
	void operator()(roptool::ast::namespace_identifier const& ast)
	{
		identifiers.insert(ast);
	}

	void operator()(roptool::ast::operation const& ast)
	{
		boost::apply_visitor(*this, ast.operand_);
	}

	void operator()(roptool::ast::unary_operand const& ast)
	{
		boost::apply_visitor(*this, ast.operand_);
	}

	void operator()(roptool::ast::expression const& ast)
	{
		boost::apply_visitor(*this, ast.first);

		for (auto const& oper : ast.rest)
			(*this)(oper);
	}

	IdentifierList identifiers;
};


class ExpressionDataReference
{
public:
	using IdentifierLookup = std::function<DataRefPtr(roptool::ast::namespace_identifier const&)>;

public:
	ExpressionDataReference() = default;

	void set(roptool::ast::expression const& expr, IdentifierLookup id_lookup)
	{
		// search tree for all identifiers
		ExpressionIdentifierCrawler crawler;
		crawler(expr);

		auto reducer = [this, &expr, id_lookup](u64 oldval, u64 newval)
		{
			ExpressionReducer expr_reducer(id_lookup);
			m_data_ref->set(expr_reducer(expr));
		};

		for (auto const& identifier : crawler.identifiers)
		{
			// TODO: need to check local scope!!!
			auto ref = id_lookup(identifier);

			if (ref->constant())
				continue;

			ref->on_update(reducer);
			m_constant = false;
		}

		ExpressionReducer expr_reducer(id_lookup);
		auto initial_value = expr_reducer(expr);
		m_data_ref = std::make_shared<DataReference>(initial_value, m_constant);
	}

	DataRefPtr get(void)
	{
		return m_data_ref;
	}

private:
	DataRefPtr m_data_ref;	
	bool m_constant{true};
};

#endif // EXPRESSIONDATAREFERENCE_H
