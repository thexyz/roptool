#ifndef _ROP_FUNCTION_H_
#define _ROP_FUNCTION_H_

// roptool
#include "RopFunctionCall.h"
#include "Target.h"
#include "DataRef.h"
#include "relocation.h"

// std
#include <memory>
#include <list>

class RopFunction
{
    public:
		RopFunction(void);
        const std::string& name(void);
        void setName(const std::string& name);
		void set_parameters(DataRefPtrList params, const std::string& types);
		const DataRefPtrList& parameters(void);
		const std::string& parameter_types(void);
        void add(const RopFunctionCallPtr& call);
		size_t size(TargetPtr target);
        CompiledCode binary(TargetPtr target);
        RopFunctionCallPtrList &calls(void);
		
		DataRefPtr data_reference(void);
		bool data_reference_used(void);
		DataRefPtr data_size_reference(void);
		bool data_size_reference_used(void);
		
		DataRefPtr working_stack_index(void);
		DataRefPtr working_stack(void);
		
    private:
        std::string m_name;
        RopFunctionCallPtrList m_calls;
		DataRefPtrList m_parameters;
		std::string m_param_types;
		DataRefPtr m_data_ref, m_data_size_ref, m_working_stack_index, m_working_stack;
		bool m_data_ref_used, m_data_size_ref_used;
};

typedef std::shared_ptr<RopFunction> RopFunctionPtr;
typedef std::list<RopFunctionPtr> RopFunctionPtrList;

#endif // _ROP_FUNCTION_H_
