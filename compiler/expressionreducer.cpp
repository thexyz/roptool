#include "expressionreducer.h"


DataRefPtr ExpressionReducer::operator()(roptool::ast::u64 const& ast) const
{
	return std::make_shared<DataReference>(ast, true);
}

DataRefPtr ExpressionReducer::operator()(roptool::ast::u32 const& ast) const
{
	return std::make_shared<DataReference>(ast, true);
}

DataRefPtr ExpressionReducer::operator()(roptool::ast::u16 const& ast) const
{
	return std::make_shared<DataReference>(ast, true);
}

DataRefPtr ExpressionReducer::operator()(roptool::ast::u8 const& ast) const
{
	return std::make_shared<DataReference>(ast, true);
}

DataRefPtr ExpressionReducer::operator()(roptool::ast::constant const& ast) const
{
	return boost::apply_visitor(*this, ast);
}

DataRefPtr ExpressionReducer::operator()(roptool::ast::namespace_identifier const& ast) const
{
	// TODO: need to check local scope!!!
	return m_id_lookup(ast);
}

DataRefPtr ExpressionReducer::operator()(DataRefPtr lhs, roptool::ast::operation const& ast) const
{
	auto rhs = boost::apply_visitor(*this, ast.operand_);
	auto res = std::make_shared<DataReference>();
	
	switch (ast.operator_)
	{
		case roptool::ast::expression_operator::addition:
            res->set(lhs, rhs, DataReferenceOperation::ADDITION);
			break;
			
		case roptool::ast::expression_operator::subtraction: 
            res->set(lhs, rhs, DataReferenceOperation::SUBTRACTION);
			break;
			
		case roptool::ast::expression_operator::multiplication:
            res->set(lhs, rhs, DataReferenceOperation::MULTIPLICATION);
			break;
			
		case roptool::ast::expression_operator::division:
            res->set(lhs, rhs, DataReferenceOperation::DIVISION);
			break;
			

		default:
		   BOOST_ASSERT(0);
			break;
	}
	
	return res;
}

DataRefPtr ExpressionReducer::operator()(roptool::ast::unary_operand const& ast) const
{
	auto value = boost::apply_visitor(*this, ast.operand_);
	switch (ast.operator_)
	{
		case roptool::ast::unary_operator::positive: 
			return value;
		case roptool::ast::unary_operator::negative:
		{
			auto neg = std::make_shared<DataReference>();
            neg->set(value, DataReferenceUnaryOperation::NEGATIVE);
			return neg;
		}
		
		case roptool::ast::unary_operator::ones_complement:
		{
			auto ones_comp = std::make_shared<DataReference>();
            ones_comp->set(value, DataReferenceUnaryOperation::ONES_COMPLEMENT);
			return ones_comp;
		}

		default:
		   BOOST_ASSERT(0);
		   return nullptr;
	}
}

DataRefPtr ExpressionReducer::operator()(roptool::ast::expression const& ast) const
{	
	auto lhs = boost::apply_visitor(*this, ast.first);

	for (auto const& oper : ast.rest)
		lhs = (*this)(lhs, oper);

	return lhs;
}
