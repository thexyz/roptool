// roptool
#include "XmlGadget.h"

// std
#include <cstdlib>
#include <stdexcept>

// boost
#include <boost/filesystem.hpp>

// tinyxml2
#include <tinyxml2/tinyxml2.h>

XmlGadget::XmlGadget(void)
	: m_visitor(new XmlActionVisitor)
{
    // add handlers
    m_visitor->addHandler("gadget-address", [this](const auto& address)
	{
		m_address = std::make_shared<DataReference>(std::strtoull(address.c_str(), NULL, 0), true);
	}, true);
}

bool XmlGadget::parse(const std::string& file)
{
    // create new XML document
	tinyxml2::XMLDocument xmldoc;
    
    // load XML file
    if (xmldoc.LoadFile(file.c_str()) != tinyxml2::XML_NO_ERROR)
    {
        // \TODO: better error message
        // error reading file
        throw std::runtime_error(std::string("Could not open manifest file: ") + file);
    }
    
    // set name
    m_name = boost::filesystem::path(file).filename().replace_extension().string();
    
    // use visitor through XML
    xmldoc.Accept(m_visitor.get());
	on_parse_complete();
    return true;
}

DataRefPtr XmlGadget::address(void) const
{
    return m_address;
}

const std::string& XmlGadget::name(void) const
{
    return m_name;
}

XmlActionVisitor *XmlGadget::visitor(void)
{
	return  m_visitor.get();
}

void XmlGadget::on_parse_complete(void)
{
	// do nothing
}