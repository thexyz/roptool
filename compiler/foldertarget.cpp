// roptool
#include "foldertarget.h"
#include "XmlGadget.h"
#include "XmlGadgetMap.h"
#include "xmltargetmanifest.h"
#include "xmlaslrgadget.h"

// boost
#include <boost/filesystem.hpp>

// std
#include <iostream>

using DirectoryList = std::vector<boost::filesystem::path>;
namespace 
{
namespace fs = boost::filesystem;

DirectoryList read_directory(const fs::path& dir)
{
    DirectoryList list;
    
    // get a list of the files
    std::copy(fs::directory_iterator(dir), fs::directory_iterator(), std::back_inserter(list));
    return list;
}	
} // anonymous

FolderTarget::FolderTarget(void)
	: m_manifest(new XmlTargetManifest)
{
	
}

FolderTarget::FolderTarget(const std::string& name)
	: m_manifest(new XmlTargetManifest)
{ 
	setName(name); 
}

void FolderTarget::readManifest(void)
{
    // parse the manifest
    m_manifest->parse(m_path + "/manifest.xml");
}

void FolderTarget::readGadgets(void)
{
    // empty gadget list
    m_gadgets.clear();
    
    // read directory
    DirectoryList list = read_directory(m_path + "/gadgets");
    
    // loop through the directory list
    std::for_each(list.begin(), list.end(), [=](const fs::path& p)
    {
		XmlGadgetPtr gadget;
		
		if (m_manifest->aslr())
		{
			gadget = std::make_shared<XmlAslrGadget<AslrTable>>(m_aslr_table);
		}
		else
		{
			gadget = std::make_shared<XmlGadget>();
		}
        
        if (fs::is_regular_file(p))
        {
            // parse gadget
            gadget->parse(p.string());
        }
        
        // add to list
        m_gadgets.push_back(gadget);
    });
}

void FolderTarget::readGadgetMaps(void)
{
    // empty gadget list
    m_gadgetmaps.clear();
    
    // read directory
    DirectoryList list = read_directory(m_path + "/gadgetmaps");
    
    // loop through the directory list
    std::for_each(list.begin(), list.end(), [=](const fs::path& p)
    {
        XmlGadgetMapPtr gadgetmap(new XmlGadgetMap);
        
        if (fs::is_regular_file(p))
        {
            // parse gadgetmap
            gadgetmap->addGadgets(m_gadgets);
            gadgetmap->parse(p.string());
            gadgetmap->setReturnGadget(getCallerGadget());
        }
        
        // add to list
        m_gadgetmaps.push_back(gadgetmap);
    });
    
    // sort by size giving priority to functions
    std::sort(m_gadgetmaps.begin(), m_gadgetmaps.end(), [=](GadgetMapPtr i, GadgetMapPtr j) -> bool
    {
        if (i->isFunction() && !j->isFunction())
        {
            return true;
        }
        
        else if (!i->isFunction() && j->isFunction())
        {
            return false;
        }
        
        // either both are function or both are not, we just compare by size
        return (i->size() < j->size());
    });
}

GadgetMapPtr FolderTarget::bestGadgetMap(const std::string& prototype)
{
    // create regular expression
    GadgetMapPtr map;
    
    // loop through gadgetmaps
    auto it = std::find_if(m_gadgetmaps.begin(), m_gadgetmaps.end(), [=](GadgetMapPtr map) -> bool
    {
        boost::cmatch match;
        
        // first element that is valid should be smallest since list is sorted
        return (boost::regex_match(prototype.c_str(), match, map->regex()));
    });
    
    // check if beyond range
    if (it == m_gadgetmaps.end())
    {
        // no gadget found!
        return map;
    }
    
    // copy the gadgetmap
    map = (*it)->clone();
    return map;
}

GadgetPtr FolderTarget::getCallerGadget(void)
{
    // loop through gadgets
    auto it = std::find_if(m_gadgets.begin(), m_gadgets.end(), [=](GadgetPtr gadget) -> bool
    {
        return (gadget->name() == m_manifest->caller_gadget());
    });
    
    // check if we never got one
    if (it == m_gadgets.end())
    {
        std::string err;
        err = err + "Caller gadget \"" + m_manifest->caller_gadget() + "\" not found!";
        
        // caller gadget is not defined
        throw std::runtime_error(err.c_str()); 
    }
    
    // return the gadget
    return *it;
}

bool FolderTarget::isFunction(const std::string& function) const
{
    auto it = std::find_if(m_gadgetmaps.begin(), m_gadgetmaps.end(), [=](GadgetMapPtr map) -> bool
    {
        return (map->function() == function);
    });
    
    // true if we found a match
    return !(it == m_gadgetmaps.end());
}

void FolderTarget::setName(const std::string& name)
{
    // set name
    m_name = name;
    m_path = target_folder() + "/" + name;
    
    // read manifest
    readManifest();
	readGadgets();
    readGadgetMaps();
}

const AslrTable& FolderTarget::aslr_table(void) const
{
	return m_aslr_table;
}

AslrTable& FolderTarget::aslr_table(void)
{
    return m_aslr_table;
}
