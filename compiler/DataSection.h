#ifndef DATASECTION_H
#define DATASECTION_H

// roptool
#include "Target.h"
#include "Types.h"
#include "DataRef.h"
#include "relocation.h"

// std
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <functional>

class DataSection
{
public:
	using ReferenceModifier = std::function<DataRefPtr(DataRefPtr)>;
public:
	DataSection(void);
	
	void setBase(u64 base_address);
	
	void set_alignment(int align);
	
	// add constant
	DataRefPtr add(u64 constant);
	
	// add string
	DataRefPtr add(const std::string& str);
	
	// add raw data
	DataRefPtr add(const void *data, u64 size);

	// add raw data with a dataref
	DataRefPtr add(DataRefPtr data, u64 size);

	// add data based off several data refs
	DataRefPtr add(DataRefPtrList& dataref, u64 size);
	DataRefPtr add(DataRefPtrList& dataref, u64 size, ReferenceModifier modifier);
	
	// create reference to buffer to update
	DataRefPtr deferred_buffer(u64 size);
	
    // get list of relocations for the data section's dynamic data
    RelocationList relocations(TargetPtr target);
    
    DataRefPtr data_section_base(void);
    
    DataRefPtr base_address(void);
    
	bool update(DataRefPtr ref, const void *data, u64 size);
	
	// getters
	u64 size(void);
	u64 base(void);
	const std::vector<u8>& data(void);
	
private:
	using DynamicDataReference = struct
	{
		u64 offset;
		u64 size;
		DataRefPtr ref;
	};
	
	int m_padding;
	u64 m_base_address;
	std::vector<DynamicDataReference> m_dynamic_data_references;
	std::vector<unsigned char> m_data;
	std::map<std::string, DataRefPtr> m_references;
	std::map<DataRefPtr, u64> m_deferred_buffers;
	DataRefPtr m_base_addr, m_data_base;
};

typedef std::shared_ptr<DataSection> DataSectionPtr;

#endif // DATASECTION_H
