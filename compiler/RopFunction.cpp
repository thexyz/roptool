// roptool
#include "RopFunction.h"

#include <boost/iterator/filter_iterator.hpp>

RopFunction::RopFunction(void)
	: m_data_ref_used(false)
	, m_data_size_ref_used(false)
{
	m_data_ref = std::make_shared<DataReference>();
	m_data_size_ref = std::make_shared<DataReference>();
	m_working_stack_index = std::make_shared<DataReference>();
	m_working_stack = std::make_shared<DataReference>();
}

const std::string& RopFunction::name(void)
{
    return m_name;
}

void RopFunction::setName(const std::string& name)
{
    m_name = name;
}

void RopFunction::set_parameters(DataRefPtrList params, const std::string& types)
{
	m_parameters = params;
	m_param_types = types;
}

const DataRefPtrList& RopFunction::parameters(void)
{
	return m_parameters;
}

const std::string& RopFunction::parameter_types(void)
{
	return m_param_types;
}

void RopFunction::add(const RopFunctionCallPtr& call)
{
    m_calls.push_back(call);
}

RopFunctionCallPtrList &RopFunction::calls(void)
{
	return m_calls;
}

size_t RopFunction::size(TargetPtr target)
{
	size_t size = 0;
	
    // loop through every function call
    std::for_each(m_calls.begin(), m_calls.end(), [&size, target](const auto& call)
    {
		size += call->size(target);
        
        while (size % target->manifest()->stack_alignment())
        {
			size += target->manifest()->arch_bitlen()/8;
        }
    });
	
	return size;
}

CompiledCode RopFunction::binary(TargetPtr target)
{
    CompiledCode function;
    function.base = std::make_shared<DataReference>();
    
    // loop through every function call
    std::for_each(m_calls.begin(), m_calls.end(), [&](const RopFunctionCallPtr& call)
    {
        // get the binary blob
        auto call_compiled = call->binary(target);
        call_compiled.base->set(function.base, function.stack.size(), DataReferenceOperation::ADDITION);
        
        function.stack.insert(function.stack.end(), call_compiled.stack.begin(), call_compiled.stack.end());
        function.relocs.insert(function.relocs.end(), call_compiled.relocs.begin(), call_compiled.relocs.end());
        
        // add our caller gadget
        GadgetPtr caller = target->getCallerGadget();
        
        // get the endianness 
        bool little_endian = true;
        
        while (function.stack.size() % target->manifest()->stack_alignment())
        {
            u64 value = 0;
            
            if (target->manifest()->aslr())
            {
                auto aslr_table = target->aslr_table();
                auto relocs = create_relocation_for_aslr(aslr_table, caller->address());

                for (auto& reloc : relocs)
                {
                    reloc.offset = std::make_shared<DataReference>();
                    reloc.offset->set(function.base, function.stack.size(), DataReferenceOperation::ADDITION);
                }

                function.relocs.insert(function.relocs.end(), relocs.begin(), relocs.end());
                value = relocs[0].constant;
            }
            else
            {
                value = caller->address()->get();
            }
            
            // write endianness
            for (int k = 0; k < target->manifest()->arch_bitlen(); k += 8)
            {
                // is little endian?
                if (little_endian)
                {
                    // lsb first
                    function.stack.push_back((value >> k) & 0xFF);
                }
                else
                {
                    // msb fist
                    function.stack.push_back((value >> (target->manifest()->arch_bitlen() - k - 8)) & 0xFF);
                }
            }
        }
    });
    
    return function;
}


DataRefPtr RopFunction::data_reference(void)
{
	m_data_ref_used = true;
	return m_data_ref;
}

DataRefPtr RopFunction::data_size_reference(void)
{
	m_data_size_ref_used = true;
	return m_data_size_ref;
}

bool RopFunction::data_reference_used(void)
{
	return m_data_ref_used;
}

bool RopFunction::data_size_reference_used(void)
{
	return m_data_size_ref_used;
}

DataRefPtr RopFunction::working_stack_index(void)
{
	return m_working_stack_index;
}

DataRefPtr RopFunction::working_stack(void)
{
	return m_working_stack;
}
