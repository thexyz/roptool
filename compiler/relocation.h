#ifndef RELOCATION_H
#define RELOCATION_H

#include "DataRef.h"
#include "Target.h"

#include <vector>

enum class RelocationType
{
    ADD_SYMBOL,
    SUB_LHS_SYMBOL,
    SUB_RHS_SYMBOL,
    STORE_SYMBOL
};

struct Relocation
{
    RelocationType type;
    DataRefPtr offset;
    u64 constant;
    DataReference *aslr_ref;
};

using RelocationList = std::vector<Relocation>;

struct CompiledCode
{
    DataRefPtr base;
    std::vector<u8> stack;
    RelocationList relocs;
};

RelocationList create_relocation_for_aslr(AslrTable& aslr_table, DataReference *ref);
RelocationList create_relocation_for_aslr(AslrTable& aslr_table, DataRefPtr ref);

#endif // RELOCATION_H
