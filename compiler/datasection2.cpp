class DeferredDataReference
{
public:
	using UpdateHandler = std::function<void(u64)>;

public:
	u64 get(void);
	void set(u64 value);

	void register_handler(UpdateHandler handler);

private:
	u64 m_value;
	std::list<UpdateHandler> m_handlers;
};

class ExpressionDataReference
{
public:
	ExpressionDataReference(const DataSection *data_section)
		: m_data_section(data_section)
		, m_data_ref(nullptr)
		, m_constant(true)
	{
		m_data_ref->set(0);
	}

	void set(roptool::ast::expression const& expr)
	{
		// search tree for all identifiers
		ExpressionIdentifierCrawler crawler;
		crawler(expr);

		auto reducer = [this, &expr](u64 value)
		{
			ExpressionReducer reducer(m_data_section);
			m_data_ref->set(reducer(expr));
		};

		for (auto const& identifier : std::cref(crawler.identifiers))
		{
			// TODO: need to check local scope!!!
			DeferredDataReference *ref = m_data_section->get(identifier.namespace_, identifier.name);

			if (ref == nullptr)
			{
				// TODO: handle
				return;
			}

			if (ref->constant())
				continue;

			ref->register_handler(reducer);
			m_constant = false;
		}

		ExpressionReducer expr_reducer(m_data_section);
		auto initial_value = expr_reducer(expr);
		m_data_ref = new DataReference(initial_value, m_constant);
	}

	DeferredDataReference *get(void)
	{
		return m_data_ref;
	}

private:
	const DataSection *data_section;
	DeferredDataReference *m_data_ref{nullptr};	
	bool m_constant{true};
};

struct ExpressionIdentifierCrawler
{
	using IdentifierList = std::set<roptool::ast::namespace_identifier>;

	void operator()(roptool::ast::constant const& ast) const { }
	
	void operator()(roptool::ast::namespace_identifier const& ast) const
	{
		identifiers.insert(ast);
	}

	void operator()(roptool::ast::operation const& ast) const
	{
		boost::apply_visitor(*this, ast.operand_);
	}

	void operator()(roptool::ast::unary_operand const& ast) const
	{
		boost::apply_visitor(*this, ast.operand_);
	}

	void operator()(roptool::ast::expression const& ast) const
	{
		identifiers.clear();
		boost::apply_visitor(*this, ast.first);

		for (auto const& oper : ast.rest)
			(*this)(oper);
	}

	IdentifierList identifiers;
};

class ExpressionReducer
{
public:
	ExpressionReducer(const DataSection *data_section)
		: m_data_section(data_section)
	{
	}

	u64 operator()(roptool::ast::u64 const& ast) const;
	u64 operator()(roptool::ast::u32 const& ast) const;
	u64 operator()(roptool::ast::u16 const& ast) const;
	u64 operator()(roptool::ast::u8 const& ast) const;
	u64 operator()(roptool::ast::constant const& ast) const;
	u64 operator()(roptool::ast::namespace_identifier const& ast) const;
	u64 operator()(roptool::ast::operation const& ast) const;
	u64 operator()(roptool::ast::unary_operand const& ast) const;
	u64 operator()(roptool::ast::expression const& ast) const;

private:
	const DataSection *data_section;
};

void ExpressionReducer::operator()(roptool::ast::u64 const& ast) const
{
	return ast;
}

void ExpressionReducer::operator()(roptool::ast::u32 const& ast) const
{
	return ast;
}

void ExpressionReducer::operator()(roptool::ast::u16 const& ast) const
{
	return ast;
}

void ExpressionReducer::operator()(roptool::ast::u8 const& ast) const
{
	return ast;
}

void ExpressionReducer::operator()(roptool::ast::constant const& ast) const
{
	return boost::apply_visitor(*this, ast);
}

void ExpressionReducer::operator()(roptool::ast::namespace_identifier const& ast) const
{
	// TODO: need to check local scope!!!
	DeferredDataReference *ref = m_data_section->get(identifier.namespace_, identifier.name);

	if (ref == nullptr)
	{
		// TODO: handle
		return 0;
	}

	return ref->value();
}

void ExpressionReducer::operator()(u64 lhs, roptool::ast::operation const& ast) const
{
	auto rhs = boost::apply_visitor(*this, ast.operand_);

	switch (ast.operator_)
	{
		case roptool::ast::expression_operator::addition: 
			return lhs + rhs;
		case roptool::ast::expression_operator::subtraction: 
			return lhs - rhs;
		case roptool::ast::expression_operator::multiplication:
			return lhs * rhs;
		case roptool::ast::expression_operator::division:
			return lhs / rhs;

		default:
		   BOOST_ASSERT(0);
		   return -1;
	}
}

void ExpressionReducer::operator()(roptool::ast::unary_operand const& ast) const
{
	auto value = boost::apply_visitor(*this, ast.operand_);
	switch (ast.operator_)
	{
		case roptool::ast::unary_operator::positive: 
			return value;
		case roptool::ast::unary_operator::negative: 
			return -value;
		case roptool::ast::unary_operator::ones_complement: 
			return ~value;

		default:
		   BOOST_ASSERT(0);
		   return -1;
	}
}

void ExpressionReducer::operator()(roptool::ast::expression const& ast) const
{	
	auto lhs = boost::apply_visitor(*this, ast.first);

	for (auto const& oper : ast.rest)
		(*this)(lhs, oper);
}
