// roptool
#include "RopFunctionCall.h"

#include <boost/iterator/filter_iterator.hpp>

#include <iostream>

void RopFunctionCall::setMap(GadgetMapPtr map)
{
    m_map = map;
}

void RopFunctionCall::setFunction(Function func)
{
    m_func = func;
}

void RopFunctionCall::setParameters(DataRefPtrList refs)
{
    m_refs = refs;
}

size_t RopFunctionCall::size(TargetPtr target)
{
	return m_map->size()*(target->manifest()->arch_bitlen()/8);
}

CompiledCode RopFunctionCall::binary(TargetPtr target)
{
    CompiledCode binary;
    
    binary.base = std::make_shared<DataReference>();
    
    // set map parameters
    m_map->setFunction(m_func);
    m_map->setParameters(m_refs);
    
    // get the stack data
    auto stack = m_map->stack();
    
    // \todo: automatically obtain this
    auto little_endian = true;
    auto aslr_table = target->aslr_table();
    
    // loop through the stack and convert it to what we need
    for (unsigned int i = 0; i < stack.size(); i++)
    {
		// TODO: replace
        auto ref = stack.at(i);
        u64 stack_val = 0;
        
        if (target->manifest()->aslr() && !ref->constant())
        {
            // generate reloc information
            auto relocs = create_relocation_for_aslr(aslr_table, ref);
            
            for (auto& reloc : relocs)
            {
                reloc.offset = std::make_shared<DataReference>();
                reloc.offset->set(binary.base, i * target->manifest()->arch_bitlen()/8, DataReferenceOperation::ADDITION);
            }
            
            stack_val = relocs[0].constant;
            binary.relocs.insert(binary.relocs.end(), relocs.begin(), relocs.end());
        }
        else
        {
            stack_val = stack.at(i)->get();
        }
		
        // write endianness
        for (int k = 0; k < target->manifest()->arch_bitlen(); k += 8)
        {
            // is little endian?
            if (little_endian)
            {
                // lsb first
                binary.stack.push_back((stack_val >> k) & 0xFF);
            }
            else
            {
                // msb fist
                binary.stack.push_back((stack_val >> (target->manifest()->arch_bitlen() - k - 8)) & 0xFF);
            }
        }
    }
    
    // return the binary representation of this gadget
    return binary;
}
